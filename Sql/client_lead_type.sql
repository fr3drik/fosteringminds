CREATE TABLE `client_lead_type` (
	`client_lead_type_id` INT(11) NOT NULL AUTO_INCREMENT,
	`client_lead_type_description` VARCHAR(50) NOT NULL,
	`date_created` DATETIME NULL DEFAULT NULL,
	`date_updated` DATETIME NULL DEFAULT NULL,
	`modified_by_user_id` VARCHAR(256) NULL DEFAULT NULL,
	PRIMARY KEY (`client_lead_type_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=3
;
