CREATE TABLE `client_lead` (
	`health_care_professional_id` INT(11) NOT NULL,
	`client_lead_type_id` INT(11) NOT NULL DEFAULT '0',
	`client_lead_id` INT(11) NOT NULL AUTO_INCREMENT,
	`client_first_name` VARCHAR(255) NOT NULL,
	`client_surname` VARCHAR(255) NOT NULL,
	`contact_number` VARCHAR(255) NOT NULL,
	`email_address` VARCHAR(255) NULL DEFAULT NULL,
	`message` TEXT NULL,
	`geolocation` TEXT NULL,
	`referrer` TEXT NULL,
	`lead_status` TINYINT(4) NULL DEFAULT NULL,
	`date_created` DATETIME NULL DEFAULT NULL,
	`date_updated` DATETIME NULL DEFAULT NULL,
	`modified_by_user_id` VARCHAR(256) NULL DEFAULT NULL,
	PRIMARY KEY (`client_lead_id`),
	INDEX `client_lead_types` (`client_lead_type_id`),
	INDEX `health_care_client_leads` (`health_care_professional_id`),
	CONSTRAINT `client_lead_types` FOREIGN KEY (`client_lead_type_id`) REFERENCES `client_lead_type` (`client_lead_type_id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `health_care_client_leads` FOREIGN KEY (`health_care_professional_id`) REFERENCES `health_care_professional` (`health_care_professional_id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=36
;
