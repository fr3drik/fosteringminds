CREATE DATABASE `FosteringMinds` /*!40100 COLLATE 'latin1_swedish_ci' */

CREATE USER 'fosteringminds'@'localhost' IDENTIFIED BY 'Password@123';
GRANT EXECUTE, DELETE, SELECT, CREATE TEMPORARY TABLES, DROP, INSERT, UPDATE  ON *.* TO 'fosteringminds'@'localhost';