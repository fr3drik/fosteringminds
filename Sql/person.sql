CREATE TABLE account(
        `account_key` VARCHAR(255) NOT NULL,
        `account_reference` VARCHAR(255) NOT NULL,
        `date_created` DATETIME NULL,
        `date_updated` DATETIME NULL,        
        `modified_by_user_id` VARCHAR(256) NULL DEFAULT NULL,
        UNIQUE KEY `ix_account` (`account_reference`),
        PRIMARY KEY (`account_key`)
)
    COLLATE='latin1_swedish_ci'
    ENGINE=InnoDB
    AUTO_INCREMENT=1; 

CREATE TABLE person(
        `person_key` VARCHAR(255) NOT NULL,
        `person_names` VARCHAR(255) NOT NULL,
        `person_last_name` VARCHAR(255) NOT NULL,
        `person_identity_number` VARCHAR(50) NULL,
        `person_type` INT NOT NULL,
        `date_created` DATETIME NULL,
        `date_updated` DATETIME NULL,
        `modified_by_user_id` VARCHAR(256) NULL DEFAULT NULL,
        PRIMARY KEY (`person_key`)
    ) 
    COLLATE='latin1_swedish_ci'
    ENGINE=InnoDB
    AUTO_INCREMENT=1; 

CREATE TABLE account_person(
    `account_person_key` VARCHAR(255) NOT NULL,
    `account_key` VARCHAR(255) NOT NULL,
    `person_key` VARCHAR(255) NOT NULL,
    `date_created` DATETIME NULL,
    `date_updated` DATETIME NULL,
    `modified_by_user_id` VARCHAR(256) NULL DEFAULT NULL,
    PRIMARY KEY(`account_person_key`),
    CONSTRAINT `account_persons` FOREIGN KEY (`account_key`) REFERENCES `account` (`account_key`) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT `person_accounts` FOREIGN KEY (`person_key`) REFERENCES `person` (`person_key`) ON UPDATE CASCADE ON DELETE CASCADE    
)   
    COLLATE='latin1_swedish_ci'
    ENGINE=InnoDB
    AUTO_INCREMENT=1;  

CREATE TABLE course(
        `course_key` VARCHAR(255) NOT NULL,
        `course_title` VARCHAR(255) NOT NULL,
        `date_created` DATETIME NULL,
        `date_updated` DATETIME NULL,
        `modified_by_user_id` VARCHAR(256) NULL DEFAULT NULL,
        PRIMARY KEY (`course_key`)
    )    
    COLLATE='latin1_swedish_ci'
    ENGINE=InnoDB
    AUTO_INCREMENT=1; 

CREATE TABLE course_person_task(
    `task_key` VARCHAR(255) NOT NULL,    
    `person_key` VARCHAR(255) NOT NULL,
    `course_key` VARCHAR(255) NOT NULL,
    `task_description` LONGTEXT NOT NULL,
    `date_created` DATETIME NULL,
    `date_updated` DATETIME NULL,
    `modified_by_user_id` VARCHAR(256) NULL DEFAULT NULL,
    PRIMARY KEY (`task_key`),
    CONSTRAINT `task_persons` FOREIGN KEY (`person_key`) REFERENCES `person` (`person_key`) ON UPDATE CASCADE ON DELETE CASCADE,   
    CONSTRAINT `task_courses` FOREIGN KEY (`course_key`) REFERENCES `course` (`course_key`) ON UPDATE CASCADE ON DELETE CASCADE   
    )    
    COLLATE='latin1_swedish_ci'
    ENGINE=InnoDB
    AUTO_INCREMENT=1; 

CREATE TABLE curriculum(
    `curriculum_key` VARCHAR(255) NOT NULL,
    `curriculum_name` VARCHAR(255) NOT NULL,
    `date_created` DATETIME NULL,
    `date_updated` DATETIME NULL,
    `modified_by_user_id` VARCHAR(256) NULL DEFAULT NULL,
    PRIMARY KEY(`curriculum_key`)
)    
    COLLATE='latin1_swedish_ci'
    ENGINE=InnoDB
    AUTO_INCREMENT=1; 

CREATE TABLE curriculum_course(
    `curriculum_course_key` VARCHAR(255) NOT NULL,
    `course_key` VARCHAR(255) NOT NULL,
    `curriculum_key` VARCHAR(255) NOT NULL,
    `modified_by_user_id` VARCHAR(256) NULL DEFAULT NULL,
    PRIMARY KEY(`curriculum_course_key`),
    CONSTRAINT `course_curricula` FOREIGN KEY (`course_key`) REFERENCES `course` (`course_key`) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT `curricula_courses` FOREIGN KEY (`curriculum_key`) REFERENCES `curriculum` (`curriculum_key`) ON UPDATE CASCADE ON DELETE CASCADE
)
    COLLATE='latin1_swedish_ci'
    ENGINE=InnoDB
    AUTO_INCREMENT=1; 
 