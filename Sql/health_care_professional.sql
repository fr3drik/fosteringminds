CREATE TABLE `health_care_professional` (
	`health_care_professional_id` INT(11) NOT NULL AUTO_INCREMENT,
	`health_care_professional_first_name` VARCHAR(255) NOT NULL,
	`health_care_professional_last_name` VARCHAR(255) NOT NULL,
	`health_care_professional_title` VARCHAR(255) NULL DEFAULT NULL,
	`health_care_professional_bio` LONGTEXT NULL,
	`health_care_professional_practice_number` VARCHAR(255) NULL DEFAULT NULL,
	`health_care_professional_url` VARCHAR(255) NULL DEFAULT NULL,
	`date_created` DATETIME NULL DEFAULT NULL,
	`date_updated` DATETIME NULL DEFAULT NULL,
	`modified_by_user_id` VARCHAR(256) NULL DEFAULT NULL,
	PRIMARY KEY (`health_care_professional_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=105
;
