CREATE TABLE `health_care_professional_accreditation` (
	`health_care_professional_id` INT(11) NOT NULL,
	`health_care_professional_accreditation_id` INT(11) NOT NULL AUTO_INCREMENT,
	`health_care_professional_accreditation_title` VARCHAR(255) NOT NULL,
	`health_care_professional_accreditation_organisation` VARCHAR(255) NOT NULL,
	`date_created` DATETIME NULL DEFAULT NULL,
	`date_updated` DATETIME NULL DEFAULT NULL,
	`modified_by_user_id` VARCHAR(256) NULL DEFAULT NULL,
	PRIMARY KEY (`health_care_professional_accreditation_id`),
	INDEX `health_care_professional_accreditations` (`health_care_professional_id`),
	CONSTRAINT `health_care_professional_accreditations` FOREIGN KEY (`health_care_professional_id`) REFERENCES `health_care_professional` (`health_care_professional_id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=8
;
