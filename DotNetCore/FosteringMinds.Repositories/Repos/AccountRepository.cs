﻿using FosteringMinds.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Dapper;
using Microsoft.Extensions.Logging;
using System.Linq;
using DataAccess;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace FosteringMinds.Repositories
{
	public class AccountRepository<T> : BaseRepository<T>, IAccountRepository<T> where T : Account
	{
		public AccountRepository(ILogger<AccountRepository<T>> logger, IConfiguration configuration)
			: base(logger, configuration)
		{

		}	

		public Task<Account> SaveAccount(Account account)
		{
			try
			{
				logger.LogDebug("Saving account: {@account}", account);
				return dbManager.DbConnection.QueryFirstAsync<Account>(
					@"insert into account
						(
							account_key, 
							account_reference, 
							date_created, 
							date_updated, 
							modified_by_user_id
						)
						values
						(
							@accountKey,
							@accountReference,
							@dateCreated,
							@dateUpdated,
							@modifiedByUserId
						);
						select 
							account_key As AccountKey, 
							account_reference As AccountReference, 
							date_created As DateCreated, 
							date_updated As DateUpdated, 
							modified_by_user_id As ModifiedByUserId 
						from 
							account
						where
							account_key=@accountKey;", new { accountKey = account.AccountKey, accountReference = account.AccountReference, dateCreated = account.DateCreated, dateUpdated = account.DateUpdated, modifiedByUserId = account.ModifiedByUserId });
			}
			catch (Exception ex)
			{
				logger.LogError(ex, "An error occurred saving an account: {@account}", account);
				throw ex;
			}
		}
	}
}
