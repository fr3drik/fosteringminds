﻿using FosteringMinds.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Dapper;
using Microsoft.Extensions.Logging;
using System.Linq;
using DataAccess;
using Microsoft.Extensions.Configuration;

namespace FosteringMinds.Repositories
{
	public class ClientLeadRepository<T> : BaseRepository<T>, IClientLeadRepository<T> where T : ClientLead
	{
		public ClientLeadRepository(ILogger<ClientLeadRepository<T>> logger, IConfiguration configuration)
			: base(logger, configuration)
		{

		}

		public ClientLead GetClientLeadById(int healthCareProfessionalId, int clientLeadId)
		{
			throw new NotImplementedException();
		}

		public IList<ClientLead> GetClientLeads()
		{
			throw new NotImplementedException();
		}

		public IList<ClientLead> GetClientLeadsByHealthCareProfessional(int healthCareProfessionalId)
		{
			throw new NotImplementedException();
		}

		public ClientLeadType GetClientLeadTypeByDescription(string clientLeadTypeDescription)
		{
			throw new NotImplementedException();
		}

		public ClientLeadType GetClientLeadTypeById(int clientLeadTypeId)
		{
			throw new NotImplementedException();
		}

		public IList<ClientLeadType> GetClientLeadTypes()
		{
			throw new NotImplementedException();
		}

		public IList<ClientLeadType> GetClientLeadTypesByDescription(string clientLeadTypeDescription)
		{
			throw new NotImplementedException();
		}

		public IList<ClientLeadType> GetClientLeadTypesByHealthCareProfessional(int healthCareProfessionalId)
		{
			throw new NotImplementedException();
		}

		public ClientLead SaveClientLead(ClientLead clientLead)
		{
			var validationContext = new ValidationContext(clientLead, null, null);
			validationResults = new List<ValidationResult>();
			Validator.TryValidateObject(clientLead, validationContext, validationResults);
			if (ValidationResults.Count == 0)
			{
				if (clientLead.ClientLeadId > 0)
				{
					try
					{
						logger.LogInformation(new EventId((int)Events.SAVECLIENTLEADUPDATE), EventLibrary.EventDictionary[Events.SAVECLIENTLEADUPDATE], null);
						return DbManager.DbConnection.Query<ClientLead>(ClientLeadConstants.CLIENTLEADUPDATE, new
						{
							healthcareprofessionalid = clientLead.HealthCareProfessionalId,
							clientleadtypeid = clientLead.ClientLeadTypeId,
							clientleadid = clientLead.ClientLeadId,
							clientfirstname = clientLead.ClientFirstName,
							clientsurname = clientLead.ClientSurname,
							contactnumber = clientLead.ContactNumber,
							emailaddress = clientLead.EmailAddress,
							message = clientLead.Message,
							geolocation = clientLead.GeoLocation,
							referrer = clientLead.Referrer,
							leadstatus = clientLead.LeadStatus,
							modifiedbyuserid = clientLead.ModifiedByUserId
						}).FirstOrDefault();
					}
					catch (Exception e)
					{
						logger.LogError(new EventId((int)Events.SAVECLIENTLEADERROR), e, EventLibrary.EventDictionary[Events.SAVECLIENTLEADERROR]);
						ValidationResults.Add(new ValidationResult(EventLibrary.EventDictionary[Events.SYSTEMERROR],
						  new List<string> { EventLibrary.EventDictionary[Events.SAVECLIENTLEADERROR] }));
					}
				}
				try
				{
					logger.LogInformation(new EventId((int)Events.SAVECLIENTLEADINSERT), EventLibrary.EventDictionary[Events.SAVECLIENTLEADINSERT], null);
					return DbManager.DbConnection.Query<ClientLead>(ClientLeadConstants.CLIENTLEADINSERT, new
					{
						healthcareprofessionalid = clientLead.HealthCareProfessionalId,
						clientleadtypeid = clientLead.ClientLeadTypeId,
						clientfirstname = clientLead.ClientFirstName,
						clientsurname = clientLead.ClientSurname,
						contactnumber = clientLead.ContactNumber,
						emailaddress = clientLead.EmailAddress,
						message = clientLead.Message,
						geolocation = clientLead.GeoLocation,
						referrer = clientLead.Referrer,
						leadstatus = clientLead.LeadStatus,
						modifiedbyuserid = clientLead.ModifiedByUserId
					}).FirstOrDefault();
				}
				catch (Exception e)
				{
					logger.LogError(new EventId((int)Events.SAVECLIENTLEADERROR), e, EventLibrary.EventDictionary[Events.SAVECLIENTLEADERROR]);
					ValidationResults.Add(new ValidationResult(EventLibrary.EventDictionary[Events.SYSTEMERROR],
					  new List<string> { EventLibrary.EventDictionary[Events.SAVECLIENTLEADERROR] }));
				}
			}
			return null;
		}

		public ClientLeadType SaveClientLeadType(ClientLeadType clientLeadType)
		{
			throw new NotImplementedException();
		}
	}
}
