﻿using FosteringMinds.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Dapper;
using System.Linq;
using DataAccess;

namespace FosteringMinds.Repositories
{
	public class HealthCareProfessionalRepository<T> : BaseRepository<T>, IHealthCareProfessionalRepository<T> where T : HealthCareProfessional
	{
		public HealthCareProfessionalRepository(ILogger<HealthCareProfessionalRepository<T>> logger, IConfiguration configuration)
			:base(logger, configuration)
		{
		}

		//Accreditations
		public IList<HealthCareProfessionalAccreditation> SaveHealthCareProfessionalAccreditation(HealthCareProfessionalAccreditation healthCareProfessionalAccreditation)
		{
			var healthCareProfessionalAccreditations = DbManager.DbConnection.Query<HealthCareProfessionalAccreditation>(HealthCareProfessionalAccreditationConstants.HEALTHCAREPROFESSIONALACCREDITATIONSBYHEALTHCAREPROFESSIONAL, new { healthcareprofessionalid = healthCareProfessionalAccreditation.HealthCareProfessionalId }).ToList();

			var validationContext = new ValidationContext(healthCareProfessionalAccreditation, null, null);
			validationResults = new List<ValidationResult>();
			Validator.TryValidateObject(healthCareProfessionalAccreditation, validationContext, validationResults);

			if (healthCareProfessionalAccreditations.Where(h => h.HealthCareProfessionalAccreditationTitle.ToLower() == healthCareProfessionalAccreditation.HealthCareProfessionalAccreditationTitle.ToLower()).Any())
			{
				logger.LogInformation(new EventId((int)Events.SAVEHEALTHCAREPROFESSIONALACCREDITATIONDUPLICATEERROR), EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALACCREDITATIONDUPLICATEERROR], null);
				validationResults.Add(new ValidationResult(EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALACCREDITATIONDUPLICATEERROR]));
				return healthCareProfessionalAccreditations;
			}

			if (validationResults.Count == 0)
			{
				if (healthCareProfessionalAccreditation.HealthCareProfessionalAccreditationId == 0)
				{
					try
					{
						logger.LogInformation(new EventId((int)Events.SAVEHEALTHCAREPROFESSIONALACCREDITATIONINSERT), EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALACCREDITATIONINSERT], null);
						using (DbManager)
						{
							return
							  DbManager.DbConnection.Query<HealthCareProfessionalAccreditation>(
								HealthCareProfessionalAccreditationConstants.HEALTHCAREPROFESSIONALACCREDITATIONSINSERT, new
								{
									healthcareprofessionalid = healthCareProfessionalAccreditation.HealthCareProfessionalId,
									healthcareprofessionalaccreditationtitle = healthCareProfessionalAccreditation.HealthCareProfessionalAccreditationTitle,
									healthcareprofessionalaccreditationorganisation = healthCareProfessionalAccreditation.HealthCareProfessionalAccreditationOrganisation,
									modifiedbyuserid = healthCareProfessionalAccreditation.ModifiedByUserId
								}).ToList();
						}
					}
					catch (Exception ex)
					{
						logger.LogError(new EventId((int)Events.SAVEHEALTHCAREPROFESSIONALACCREDITATIONERROR), ex, EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALACCREDITATIONERROR]);
						validationResults.Add(new ValidationResult(EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALACCREDITATIONERROR]));
						return healthCareProfessionalAccreditations;
					}
				}
				try
				{
					logger.LogInformation(new EventId((int)Events.SAVEHEALTHCAREPROFESSIONALACCREDITATIONUPDATE), EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALACCREDITATIONUPDATE], null);

					using (DbManager)
					{
						return DbManager.DbConnection.Query<HealthCareProfessionalAccreditation>(
						  HealthCareProfessionalAccreditationConstants.HEALTHCAREPROFESSIONALACCREDITATIONSUPDATE,
						  new
						  {
							  healthcareprofessionalaccreditationid = healthCareProfessionalAccreditation.HealthCareProfessionalAccreditationId,
							  healthcareprofessionalid = healthCareProfessionalAccreditation.HealthCareProfessionalId,
							  healthcareprofessionalaccreditationtitle = healthCareProfessionalAccreditation.HealthCareProfessionalAccreditationTitle,
							  healthcareprofessionalaccreditationorganisation = healthCareProfessionalAccreditation.HealthCareProfessionalAccreditationOrganisation,
							  modifiedbyuserid = healthCareProfessionalAccreditation.ModifiedByUserId
						  }
						  ).ToList();
					}
				}
				catch (Exception ex)
				{
					logger.LogError(new EventId((int)Events.SAVEHEALTHCAREPROFESSIONALACCREDITATIONERROR), ex, EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALACCREDITATIONERROR]);
					validationResults.Add(new ValidationResult(EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALACCREDITATIONERROR]));
					return healthCareProfessionalAccreditations;
				}
			}
			return healthCareProfessionalAccreditations;
		}

		public IList<HealthCareProfessionalAccreditation> GetHealthCareProfessionalAccreditations(int healthCareProfessionalId)
		{
			try
			{
				logger.LogInformation(new EventId((int)Events.GETHEALTHCAREPROFESSIONALACCREDITATIONSBYHEALTHCAREPRFOESSIONAL), EventLibrary.EventDictionary[Events.GETHEALTHCAREPROFESSIONALACCREDITATIONSBYHEALTHCAREPRFOESSIONAL], null);
				using (DbManager)
				{
					return DbManager.DbConnection.Query<HealthCareProfessionalAccreditation>(HealthCareProfessionalAccreditationConstants.HEALTHCAREPROFESSIONALACCREDITATIONSBYHEALTHCAREPROFESSIONAL, new { healthCareProfessionalId = healthCareProfessionalId }).ToList();
				}
			}
			catch (Exception ex)
			{
				logger.LogError(new EventId((int)Events.GETHEALTHCAREPROFESSIONALACCREDITATIONSBYHEALTHCAREPROFESSIONALERROR), ex, EventLibrary.EventDictionary[Events.GETHEALTHCAREPROFESSIONALACCREDITATIONSBYHEALTHCAREPROFESSIONALERROR]);
			}
			return null;
		}

		public IList<HealthCareProfessionalAccreditation> DeleteHealthCareProfessionalAccreditation(HealthCareProfessionalAccreditation healthCareProfessionalAccreditation)
		{
			try
			{
				logger.LogInformation(new EventId((int)Events.DELETEHEALTHCAREPROFESSIONALACCREDITATION), EventLibrary.EventDictionary[Events.DELETEHEALTHCAREPROFESSIONALACCREDITATION], null);
				using (DbManager)
				{
					return DbManager.DbConnection.Query<HealthCareProfessionalAccreditation>(HealthCareProfessionalAccreditationConstants.HEALTHCAREPROFESSIONALACCREDITATIONSDELETE, new { healthcareprofessionalid = healthCareProfessionalAccreditation.HealthCareProfessionalId, healthcareprofessionalaccreditationid = healthCareProfessionalAccreditation.HealthCareProfessionalAccreditationId }).ToList();
				}
			}
			catch(Exception ex)
			{
				logger.LogError(new EventId((int)Events.DELETEHEALTHCAREPROFESSIONALACCREDITATIONERROR), ex, EventLibrary.EventDictionary[Events.DELETEHEALTHCAREPROFESSIONALACCREDITATIONERROR]);
				return GetHealthCareProfessionalAccreditations(healthCareProfessionalAccreditation.HealthCareProfessionalId);
			}
		}

		//Professional titles
		public IList<HealthCareProfessionalTitle> SaveHealthCareProfessionalTitle(HealthCareProfessionalTitle healthCareProfessionalTitle)
		{
			var healthCareProfessionalTitles = DbManager.DbConnection.Query<HealthCareProfessionalTitle>(HealthCareProfessionalTitleConstants.HEALTHCAREPROFESSIONALTITLESBYHEALTHCAREPROFESSIONAL, new { healthcareprofessionalid = healthCareProfessionalTitle.HealthCareProfessionalId }).ToList();

			var validationContext = new ValidationContext(healthCareProfessionalTitle, null, null);
			validationResults = new List<ValidationResult>();
			Validator.TryValidateObject(healthCareProfessionalTitle, validationContext, validationResults);

			if (healthCareProfessionalTitles.Where(h => h.HealthCareProfessionalTitleDescription.ToLower() == healthCareProfessionalTitle.HealthCareProfessionalTitleDescription.ToLower()).Any())
			{
				logger.LogInformation(new EventId((int)Events.SAVEHEALTHCAREPROFESSIONALTITLEDUPLICATEERROR), EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALTITLEDUPLICATEERROR], null);
				validationResults.Add(new ValidationResult(EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALTITLEDUPLICATEERROR]));
				return healthCareProfessionalTitles;
			}

			if (validationResults.Count == 0)
			{
				if (healthCareProfessionalTitle.HealthCareProfessionalTitleId == 0)
				{
					try
					{
						logger.LogInformation(new EventId((int)Events.SAVEHEALTHCAREPROFESSIONALTITLEINSERT), EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALTITLEINSERT], null);
						using (DbManager)
						{
							return
						  DbManager.DbConnection.Query<HealthCareProfessionalTitle>(
							HealthCareProfessionalTitleConstants.HEALTHCAREPROFESSIONALTITLEINSERT, new
							{
								healthcareprofessionalid = healthCareProfessionalTitle.HealthCareProfessionalId,
								healthcareprofessionaltitledescription = healthCareProfessionalTitle.HealthCareProfessionalTitleDescription,
								modifiedbyuserid = healthCareProfessionalTitle.ModifiedByUserId
							}).ToList();
						}
					}
					catch (Exception ex)
					{
						logger.LogError(new EventId((int)Events.SAVEHEALTHCAREPROFESSIONALTITLEERROR), ex, EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALTITLEERROR]);
						validationResults.Add(new ValidationResult(EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALTITLEERROR]));
						return healthCareProfessionalTitles;
					}
				}
				try
				{
					logger.LogInformation(new EventId((int)Events.SAVEHEALTHCAREPROFESSIONALTITLEUPDATE), EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALTITLEUPDATE], null);
					using (DbManager)
					{
						return DbManager.DbConnection.Query<HealthCareProfessionalTitle>(
							HealthCareProfessionalTitleConstants.HEALTHCAREPROFESSIONALTITLEUPDATE,
							  new
							  {
								  healthcareprofessionaltitleid = healthCareProfessionalTitle.HealthCareProfessionalTitleId,
								  healthcareprofessionalid = healthCareProfessionalTitle.HealthCareProfessionalId,
								  healthcareprofessionaltitledescription = healthCareProfessionalTitle.HealthCareProfessionalTitleDescription,
								  modifiedbyuserid = healthCareProfessionalTitle.ModifiedByUserId
							  }).ToList();
					}
				}
				catch (Exception ex)
				{
					logger.LogError(new EventId((int)Events.SAVEHEALTHCAREPROFESSIONALTITLEERROR), ex, EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALTITLEERROR]);
					validationResults.Add(new ValidationResult(EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALTITLEERROR]));
					return healthCareProfessionalTitles;
				}
			}
			return healthCareProfessionalTitles;
		}

		public IList<HealthCareProfessionalTitle> GetHealthCareProfessionalTitles(int healthCareProfessionalId)
		{
			try
			{
				logger.LogInformation(new EventId((int)Events.GETHEALTHCAREPROFESSIONALTITLESBYHEALTHCAREPROFESSIONAL), EventLibrary.EventDictionary[Events.GETHEALTHCAREPROFESSIONALTITLESBYHEALTHCAREPROFESSIONAL], null);
				using (DbManager)
				{
					return DbManager.DbConnection.Query<HealthCareProfessionalTitle>(HealthCareProfessionalTitleConstants.HEALTHCAREPROFESSIONALTITLESBYHEALTHCAREPROFESSIONAL, new { healthCareProfessionalId = healthCareProfessionalId }).ToList();
				}
			}
			catch (Exception ex)
			{
				logger.LogError(new EventId((int)Events.GETHEALTHCAREPROFESSIONALTITLESBYHEALTHCAREPROFESSIONALERROR), ex, EventLibrary.EventDictionary[Events.GETHEALTHCAREPROFESSIONALTITLESBYHEALTHCAREPROFESSIONALERROR]);
			}
			return null;
		}

		public IList<HealthCareProfessionalTitle> DeleteHealthCareProfessionalTitle(HealthCareProfessionalTitle healthCareProfessionalTitle)
		{
			try
			{
				logger.LogInformation(new EventId((int)Events.DELETEHEALTHCAREPROFESSIONALTITLE), EventLibrary.EventDictionary[Events.DELETEHEALTHCAREPROFESSIONALTITLE], null);
				using (DbManager)
				{
					return DbManager.DbConnection.Query<HealthCareProfessionalTitle>(HealthCareProfessionalTitleConstants.HEALTHCAREPROFESSIONALTITLEDELETE, new { healthcareprofessionalid = healthCareProfessionalTitle.HealthCareProfessionalId, healthcareprofessionaltitleid = healthCareProfessionalTitle.HealthCareProfessionalTitleId }).ToList();
				}
			}
			catch (Exception ex)
			{
				logger.LogError(new EventId((int)Events.DELETEHEALTHCAREPROFESSIONALTITLERRROR), ex, EventLibrary.EventDictionary[Events.DELETEHEALTHCAREPROFESSIONALTITLERRROR]);
				return GetHealthCareProfessionalTitles(healthCareProfessionalTitle.HealthCareProfessionalId);
			}
		}

		//Education
		public IList<HealthCareProfessionalEducation> SaveHealthCareProfessionalEducation(HealthCareProfessionalEducation healthCareProfessionalEducation)
		{
			var healthCareProfessionalEducationList = DbManager.DbConnection.Query<HealthCareProfessionalEducation>(HealthCareProfessionalEducationConstants.HEALTHCAREPROFESSIONALEDUCATIONBYHEALTHCAREPROFESSIONAL, new { healthcareprofessionalid = healthCareProfessionalEducation.HealthCareProfessionalId }).ToList();

			var validationContext = new ValidationContext(healthCareProfessionalEducation, null, null);
			validationResults = new List<ValidationResult>();
			Validator.TryValidateObject(healthCareProfessionalEducation, validationContext, validationResults);

			if (healthCareProfessionalEducationList.Where(h => h.HealthCareProfessionalEducationDescription.ToLower() == healthCareProfessionalEducation.HealthCareProfessionalEducationDescription.ToLower() && h.HealthCareProfessionalEducationInstitution.ToLower() == healthCareProfessionalEducation.HealthCareProfessionalEducationInstitution.ToLower()).Any())
			{
				logger.LogInformation(new EventId((int)Events.SAVEHEALTHCAREPROFESSIONALEDUCATIONDUPLICATEERROR), EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALEDUCATIONDUPLICATEERROR], null);
				validationResults.Add(new ValidationResult(EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALEDUCATIONDUPLICATEERROR]));
				return healthCareProfessionalEducationList;
			}

			if (validationResults.Count == 0)
			{
				if (healthCareProfessionalEducation.HealthCareProfessionalEducationId == 0)
				{
					try
					{
						logger.LogInformation(new EventId((int)Events.SAVEHEALTHCAREPROFESSIONALEDUCATIONINSERT), EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALEDUCATIONINSERT], null);
						using (DbManager)
						{
							return
						  DbManager.DbConnection.Query<HealthCareProfessionalEducation>(
							HealthCareProfessionalEducationConstants.HEALTHCAREPROFESSIONALEDUCATIONINSERT, new
							{
								healthcareprofessionalid = healthCareProfessionalEducation.HealthCareProfessionalId,
								healthcareprofessionaleducationdescription = healthCareProfessionalEducation.HealthCareProfessionalEducationDescription,
								healthcareprofessionaleducationinstitution = healthCareProfessionalEducation.HealthCareProfessionalEducationInstitution,
								modifiedbyuserid = healthCareProfessionalEducation.ModifiedByUserId
							}).ToList();
						}
					}
					catch (Exception ex)
					{
						logger.LogError(new EventId((int)Events.SAVEHEALTHCAREPROFESSIONALEDUCATIONERROR), ex, EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALEDUCATIONERROR]);
						validationResults.Add(new ValidationResult(EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALEDUCATIONERROR]));
						return healthCareProfessionalEducationList;
					}
				}
				try
				{
					logger.LogInformation(new EventId((int)Events.SAVEHEALTHCAREPROFESSIONALEDUCATIONUPDATE), EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALEDUCATIONUPDATE], null);
					using (DbManager)
					{
						return DbManager.DbConnection.Query<HealthCareProfessionalEducation>(
					  HealthCareProfessionalEducationConstants.HEALTHCAREPROFESSIONALEDUCATIONUPDATE,
					  new
					  {
						  healthcareprofessionaleducationid = healthCareProfessionalEducation.HealthCareProfessionalEducationId,
						  healthcareprofessionalid = healthCareProfessionalEducation.HealthCareProfessionalId,
						  healthcareprofessionaleducationdescription = healthCareProfessionalEducation.HealthCareProfessionalEducationDescription,
						  healthcareprofessionaleducationinstitution = healthCareProfessionalEducation.HealthCareProfessionalEducationInstitution,
						  modifiedbyuserid = healthCareProfessionalEducation.ModifiedByUserId
					  }
					  ).ToList();
					}
				}
				catch (Exception ex)
				{
					logger.LogError(new EventId((int)Events.SAVEHEALTHCAREPROFESSIONALEDUCATIONERROR), ex, EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALEDUCATIONERROR]);
					validationResults.Add(new ValidationResult(EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALEDUCATIONERROR]));
					return healthCareProfessionalEducationList;
				}
			}
			return healthCareProfessionalEducationList;
		}

		public IList<HealthCareProfessionalEducation> GetHealthCareProfessionalEducation(int healthCareProfessionalId)
		{
			try
			{
				logger.LogInformation(new EventId((int)Events.GETHEALTHCAREPROFESSIONALEDUCATIONBYHEALTHCAREPRFOESSIONAL), EventLibrary.EventDictionary[Events.GETHEALTHCAREPROFESSIONALEDUCATIONBYHEALTHCAREPRFOESSIONAL], null);
				using (DbManager)
				{
					return DbManager.DbConnection.Query<HealthCareProfessionalEducation>(HealthCareProfessionalEducationConstants.HEALTHCAREPROFESSIONALEDUCATIONBYHEALTHCAREPROFESSIONAL, new { healthCareProfessionalId = healthCareProfessionalId }).ToList();
				}
			}
			catch (Exception ex)
			{
				logger.LogError(new EventId((int)Events.GETHEALTHCAREPROFESSIONALEDUCATIONBYHEALTHCAREPROFESSIONALERROR), ex, EventLibrary.EventDictionary[Events.GETHEALTHCAREPROFESSIONALEDUCATIONBYHEALTHCAREPROFESSIONALERROR]);
			}
			return null;
		}

		public IList<HealthCareProfessionalEducation> DeleteHealthCareProfessionalEducation(HealthCareProfessionalEducation healthCareProfessionalEducation)
		{
			try
			{
				logger.LogInformation(new EventId((int)Events.DELETEHEALTHCAREPROFESSIONALEDUCATION), EventLibrary.EventDictionary[Events.DELETEHEALTHCAREPROFESSIONALEDUCATION], null);
				using (DbManager)
				{
					return DbManager.DbConnection.Query<HealthCareProfessionalEducation>(HealthCareProfessionalEducationConstants.HEALTHCAREPROFESSIONALEDUCATIONDELETE, new { healthcareprofessionalid = healthCareProfessionalEducation.HealthCareProfessionalId, healthcareprofessionaleducationid = healthCareProfessionalEducation.HealthCareProfessionalEducationId }).ToList();
				}
			}
			catch (Exception ex)
			{
				logger.LogError(new EventId((int)Events.DELETEHEALTHCAREPROFESSIONALEDUCATIONERROR), ex, EventLibrary.EventDictionary[Events.DELETEHEALTHCAREPROFESSIONALEDUCATIONERROR]);
				return GetHealthCareProfessionalEducation(healthCareProfessionalEducation.HealthCareProfessionalId);
			}
		}

		//Health Care Professional
		public HealthCareProfessional GetHealthCareProfessionalById(int healthCareProfessionalId)
		{
			if (healthCareProfessionalId > 0)
			{
				try
				{
					logger.LogInformation(new EventId((int)Events.GETHEALTHCAREPROFESSIONALBYID), EventLibrary.EventDictionary[Events.GETHEALTHCAREPROFESSIONALBYID], null);
					using (DbManager)
					{

						using (var transaction = DbManager.DbConnection.BeginTransaction())
						{
							var healthCareProfessional = DbManager.DbConnection.Query<HealthCareProfessional>(HealthCareProfessionalConstants.HEALTHCAREPROFESSIONALBYIDSELECT, new { healthcareprofessionalid = healthCareProfessionalId }, transaction: transaction).First();

							return healthCareProfessional;
						}
					}
				}
				catch(Exception ex)
				{
					logger.LogError(new EventId((int)Events.GETHEALTHCAREPROFESSIONALBYIDERROR), ex, EventLibrary.EventDictionary[Events.GETHEALTHCAREPROFESSIONALBYIDERROR]);
				}
			}
			return null;
		}

		public IList<HealthCareProfessional> GetHealthCareProfessionals()
		{
			try
			{
				logger.LogInformation(new EventId((int)Events.GETHEALTHCAREPROFESSIONALS), EventLibrary.EventDictionary[Events.GETHEALTHCAREPROFESSIONALS], null);
				using (DbManager)
				{
					return DbManager.DbConnection.Query<HealthCareProfessional>(HealthCareProfessionalConstants.HEALTHCAREPROFESSIONALSSELECT).ToList();
				}
			}
			catch(Exception ex)
			{
				logger.LogError(new EventId((int)Events.GETHEALTHCAREPROFESSIONALSERROR), ex, EventLibrary.EventDictionary[Events.GETHEALTHCAREPROFESSIONALSERROR]);
			}
			return null;
		}

		public HealthCareProfessional SaveHealthCareProfessional(HealthCareProfessional healthCareProfessional)
		{
			var validationContext = new ValidationContext(healthCareProfessional, null, null);
			validationResults = new List<ValidationResult>();
			Validator.TryValidateObject(healthCareProfessional, validationContext, validationResults);
			if (ValidationResults.Count == 0)
			{
				if (healthCareProfessional.HealthCareProfessionalId > 0)
				{
					try
					{
						logger.LogInformation(new EventId((int)Events.SAVEHEALTHCAREPROFESSIONALUPDATE), EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALUPDATE], null);
						using (DbManager)
						{
							return DbManager.DbConnection.Query<HealthCareProfessional>(HealthCareProfessionalConstants.HEALTHCAREPROFESSIONALUPDATE, new
							{
								healthcareprofessionalfirstname = healthCareProfessional.HealthCareProfessionalFirstName,
								healthcareprofessionallastname = healthCareProfessional.HealthCareProfessionalLastName,
								healthcareprofessionaltitle = healthCareProfessional.HealthCareProfessionalTitle,
								healthcareprofessionalbio = healthCareProfessional.HealthCareProfessionalBio,
								healthcareprofessionalurl = healthCareProfessional.HealthCareProfessionalUrl,
								healthcareprofessionalid = healthCareProfessional.HealthCareProfessionalId,
								modifiedbyuserid = healthCareProfessional.ModifiedByUserId
							}).FirstOrDefault();
						}
					}
					catch (Exception e)
					{
						logger.LogError(new EventId((int)Events.SAVEHEALTHCAREPROFESSIONALERROR), e, EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALERROR]);
						ValidationResults.Add(new ValidationResult(EventLibrary.EventDictionary[Events.SYSTEMERROR],
						  new List<string> { EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALERROR] }));
					}
				}
				try
				{
					logger.LogInformation(new EventId((int)Events.SAVEHEALTHCAREPROFESSIONALINSERT), EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALINSERT], null);
					using (DbManager)
					{
						return DbManager.DbConnection.Query<HealthCareProfessional>(HealthCareProfessionalConstants.HEALTHCAREPROFESSIONALINSERT, new
						{
							healthcareprofessionalfirstname = healthCareProfessional.HealthCareProfessionalFirstName,
							healthcareprofessionallastname = healthCareProfessional.HealthCareProfessionalLastName,
							healthcareprofessionaltitle = healthCareProfessional.HealthCareProfessionalTitle,
							healthcareprofessionalbio = healthCareProfessional.HealthCareProfessionalBio,
							healthcareprofessionalurl = healthCareProfessional.HealthCareProfessionalUrl,
							modifiedbyuserid = healthCareProfessional.ModifiedByUserId
						}).FirstOrDefault();
					}
				}
				catch (Exception e)
				{
					logger.LogError(new EventId((int)Events.SAVEHEALTHCAREPROFESSIONALERROR), e, EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALERROR]);
					ValidationResults.Add(new ValidationResult(EventLibrary.EventDictionary[Events.SYSTEMERROR],
					  new List<string> { EventLibrary.EventDictionary[Events.SAVEHEALTHCAREPROFESSIONALERROR] }));
				}
			}
			return null;
		}		
	}
}
