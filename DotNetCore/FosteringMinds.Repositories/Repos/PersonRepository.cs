﻿using FosteringMinds.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Dapper;
using Microsoft.Extensions.Logging;
using System.Linq;
using DataAccess;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace FosteringMinds.Repositories
{
	public class PersonRepository<T> : BaseRepository<T>, IPersonRepository<T> where T : Person
	{
		public PersonRepository(ILogger<PersonRepository<T>> logger, IConfiguration configuration)
			: base(logger, configuration)
		{

		}

		public Task<Person> SavePerson(Person person)
		{
			try
			{
				logger.LogDebug("Saving person: {@person}", person);
				return dbManager.DbConnection.QueryFirstAsync<Person>(
					@"insert into person
					(
						person_key, 
						person_names, 
						person_last_name,
						person_identity_number, 
						person_type, 
						date_created, 
						date_updated, 
						modified_by_user_id
					)
					values
					(
						@personKey,
						@personNames,
						@personLastName,
						@personIdentityNumber,
						@personType,
						@dateCreated,
						@dateUpdated,
						@modifiedByUserId
					);
					select 
						person_key As PersonKey, 
						person_names As Names, 
						person_last_name As LastName,
						person_identity_number As IdentityNumber,
						person_type As PersonType,
						date_created As DateCreated, 
						date_updated As DateUpdated, 
						modified_by_user_id As ModifiedByUserId 
					from person
					where
						person_key = @personKey;", new
					{
						personKey = person.PersonKey,
						personNames = person.Names,
						personLastName = person.LastName,
						personIdentityNumber = person.IdentityNumber,
						personType = person.PersonType,
						dateCreated = person.DateCreated,
						dateUpdated = person.DateUpdated,
						modifiedByUserId = person.ModifiedByUserId
					});
			}
			catch (Exception ex)
			{
				logger.LogError(ex, "An error occurred saving a person: {@person}", person);
				throw ex;
			}
		}
	}
}
