﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FosteringMinds.Repositories
{
	public static class HealthCareProfessionalConstants
	{
		internal static readonly string HEALTHCAREPROFESSIONALCOLUMNS =
	 @"health_care_professional_id As HealthCareProfessionalId,
          health_care_professional_first_name As HealthCareProfessionalFirstName,
          health_care_professional_last_name As HealthCareProfessionalLastName,
          health_care_professional_title As HealthCareProfessionalTitle,
          health_care_professional_bio As HealthCareProfessionalBio,
          health_care_professional_url As HealthCareProfessionalUrl,
          date_created As DateCreated,
          date_updated As DateUpdated,
          modified_by_user_id As ModifiedByUserId";

		internal static readonly string HEALTHCAREPROFESSIONALBYIDSELECT =
		  $"select {HEALTHCAREPROFESSIONALCOLUMNS} from health_care_professional where health_care_professional_id=@healthcareprofessionalid;";

		internal static readonly string HEALTHCAREPROFESSIONALBYURLSELECT =
		  $"select {HEALTHCAREPROFESSIONALCOLUMNS} from health_care_professional where health_care_professional_url=@healthcareprofessionalurl";

		internal static readonly string HEALTHCAREPROFESSIONALBYLASTIDSELECT =
		  $"select {HEALTHCAREPROFESSIONALCOLUMNS} from health_care_professional where health_care_professional_id=LAST_INSERT_ID()";

		internal static readonly string HEALTHCAREPROFESSIONALSSELECT =
		  $"select {HEALTHCAREPROFESSIONALCOLUMNS} from health_care_professional";

		internal static readonly string HEALTHCAREPROFESSIONALUPDATE =
		  $@"update health_care_professional
            set
              health_care_professional_first_name = @healthcareprofessionalfirstname,
              health_care_professional_last_name = @healthcareprofessionallastname,
              health_care_professional_title = @healthcareprofessionaltitle,
              health_care_professional_bio = @healthcareprofessionalbio,
              health_care_professional_url = @healthcareprofessionalurl,
              date_updated = now(),
              modified_by_user_id = @modifiedbyuserid
            where
              health_care_professional_id = @healthcareprofessionalid;
        {HEALTHCAREPROFESSIONALBYIDSELECT}";

		internal static readonly string HEALTHCAREPROFESSIONALINSERT =
		  $@"insert into health_care_professional
          ( health_care_professional_first_name, 
            health_care_professional_last_name,
            health_care_professional_title,
            health_care_professional_bio, 
            health_care_professional_url,
            date_created, 
            date_updated,
            modified_by_user_id
            ) 
        values (
            @healthcareprofessionalfirstname, 
            @healthcareprofessionallastname,
            @healthcareprofessionaltitle,
            @healthcareprofessionalbio, 
            @healthcareprofessionalurl,
            now(), 
            now(), 
            @modifiedbyuserid
         );
         {HEALTHCAREPROFESSIONALBYLASTIDSELECT}";
	}

	public static class HealthCareProfessionalAccreditationConstants
	{
		internal readonly static string HEALTHCAREPROFESSIONALACCREDITATIONCOLUMNS =
		  @"
        health_care_professional_id As HealthCareProfessionalId,
        health_care_professional_accreditation_id As HealthCareProfessionalAccreditationId,
        health_care_professional_accreditation_title As HealthCareProfessionalAccreditationTitle,
        health_care_professional_accreditation_organisation As HealthCareProfessionalAccreditationOrganisation,
        date_created As DateCreated,
        date_updated As DateUpdated,
        modified_by_user_id As ModifiedByUserId
      ";
		internal readonly static string HEALTHCAREPROFESSIONALACCREDITATIONSBYHEALTHCAREPROFESSIONAL =
		  $@"
        select 
        {HEALTHCAREPROFESSIONALACCREDITATIONCOLUMNS}
        from health_care_professional_accreditation
        where
           health_care_professional_id=@healthcareprofessionalid;
      ";

		internal readonly static string HEALTHCAREPROFESSIONALACCREDITATIONSINSERT =
		  $@"
        insert into health_care_professional_accreditation
        (
          health_care_professional_id,
          health_care_professional_accreditation_title,
          health_care_professional_accreditation_organisation,
          date_created,
          date_updated,
          modified_by_user_id
        )
        values
        (
          @healthcareprofessionalid,
          @healthcareprofessionalaccreditationtitle,
          @healthcareprofessionalaccreditationorganisation,
          now(),
          now(),
          @modifiedbyuserid
        );
        {HEALTHCAREPROFESSIONALACCREDITATIONSBYHEALTHCAREPROFESSIONAL}
      ";

		internal readonly static string HEALTHCAREPROFESSIONALACCREDITATIONSUPDATE =
		  $@"
        update health_care_professional_accreditation
          set
            health_care_professional_accreditation_title = @healthcareprofessionalaccreditationtitle,
            health_care_professional_accreditation_organisation = @healthcareprofessionalaccreditationorganisation,
            date_updated = now(),
            modified_by_user_id = @modifiedbyuserid
          where
            health_care_professional_id = @healthcareprofessionalid
            and
            health_care_professional_accreditation_id = @healthcareprofessionalaccreditationid;
          {HEALTHCAREPROFESSIONALACCREDITATIONSBYHEALTHCAREPROFESSIONAL}
      ";

		internal readonly static string HEALTHCAREPROFESSIONALACCREDITATIONSDELETE =
		  $@"
        delete from health_care_professional_accreditation
        where 
          health_care_professional_id = @healthcareprofessionalid 
          and
          health_care_professional_accreditation_id = @healthcareprofessionalaccreditationid;
        {HEALTHCAREPROFESSIONALACCREDITATIONSBYHEALTHCAREPROFESSIONAL}
      ";
	}

	public static class ClientLeadConstants
	{
		internal readonly static string CLIENTLEADTYPECOLUMNS =
		  @"
          client_lead_type_id As ClientLeadTypeId, 
          client_lead_type_description As ClientLeadTypeDescription, 
          date_created As DateCreated, 
          date_updated As DateUpdated, 
          modified_by_user_id As ModifiedByUserId 
      ";

		internal readonly static string CLIENTLEADTYPESELECTBYDESCRIPTION =
	  $@"select {CLIENTLEADTYPECOLUMNS} from client_lead_type where client_lead_type_description = @clientleadtypedescription;";

		internal readonly static string CLIENTLEADTYPEINSERT =
		  $@"insert into client_lead_type
        (
          client_lead_type_description,
          date_created,
          date_updated,
          modified_by_user_id
        )
        values
        (
          @clientleadtypedescription,
          now(),
          now(),
          @modifiedbyuserid
        );
        {CLIENTLEADTYPESELECTBYDESCRIPTION}
        ";

		internal readonly static string CLIENTLEADTYPEUPDATE =
		  $@"update client_lead_type        
         set 
          client_lead_type_description = @clientleadtypedescription,
          date_updated = now()
        where 
          client_lead_type_id = @clientleadtypeid;
        {CLIENTLEADTYPESELECTBYID}
        ";

		internal readonly static string CLIENTLEADTYPESELECTBYID =
		  $@"select 
          {CLIENTLEADTYPECOLUMNS} 
        from client_lead_type 
        where client_lead_type_id = @clientleadtypeid;";

		internal readonly static string CLIENTLEADTYPESSELECT =
	  $@"select {CLIENTLEADTYPECOLUMNS} from client_lead_type;";

		internal readonly static string CLIENTLEADTYPESBYHEALTHCAREPROFESSIONALSELECT =
	$@"select {CLIENTLEADTYPECOLUMNS} from client_lead_type where health_care_professional_id=@healthcareprofessionalid;";

		internal readonly static string CLIENTLEADCOLUMNS =
		  "cl.health_care_professional_id As HealthCareProfessionalId,cl.client_lead_type_id As ClientLeadTypeId,cl.client_lead_id As ClientLeadId,cl.client_first_name As ClientFirstName,cl.client_surname As ClientSurname, cl.contact_number As ContactNumber,cl.email_address As EmailAddress,LEFT(cl.message, 256) As Message,LEFT(cl.geolocation, 256) As GeoLocation,cl.referrer As Referrer, cl.lead_status As LeadStatus, clt.client_lead_type_description as ClientLeadTypeDescription, cl.date_created As DateCreated,cl.date_updated As DateUpdated,cl.modified_by_user_id As ModifiedByUserId";

		internal readonly static string CLIENTLEADSELECTBYID =
		  $@"select {CLIENTLEADCOLUMNS} from client_lead cl join client_lead_type clt on cl.client_lead_type_id=clt.client_lead_type_id where health_care_professional_id=@healthcareprofessionalid and client_lead_id=@clientleadid";

		internal readonly static string CLIENTLEADSELECTBYLASTID =
	  $@"select {CLIENTLEADCOLUMNS} from client_lead cl join client_lead_type clt on cl.client_lead_type_id=clt.client_lead_type_id where client_lead_id=LAST_INSERT_ID()";

		internal readonly static string CLIENTLEADSBYHEALTHCAREPROFESSIONAL =
		  $@"select {CLIENTLEADCOLUMNS} from client_lead cl join client_lead_type clt on cl.client_lead_type_id=clt.client_lead_type_id  where health_care_professional_id=@healthcareprofessionalid order by cl.date_created desc";

		internal readonly static string CLIENTLEADINSERT =
		  $@"insert into client_lead
        (
          health_care_professional_id,
          client_lead_type_id,
          client_first_name,
          client_surname,
          contact_number,
          email_address,
          message,
          geolocation,
          referrer,
          lead_status,
          date_created,
          date_updated,
          modified_by_user_id
        )
        values
        (
          @healthcareprofessionalid,
          @clientleadtypeid,
          @clientfirstname,
          @clientsurname,
          @contactnumber,
          @emailaddress,
          @message,
          @geolocation,
          @referrer,
          @leadstatus,
          now(),
          now(),
          @modifiedbyuserid
        );
        {CLIENTLEADSELECTBYLASTID}
        ";
		internal readonly static string CLIENTLEADUPDATE =
	  $@"update client_lead
          set 
            client_first_name=@clientfirstname,
            client_surname = @clientsurname,
            contact_number=@contactnumber,
            email_address=@emailaddress,
            message=@message,
            geolocation=@geolocation,
            referrer=@referrer,
            lead_status=@leadstatus,
            date_updated=now(),
            modified_by_user_id=@modifiedbyuserid
          where
            health_care_professional_id = @healthcareprofessionalid and
            client_lead_id = @clientleadid and
            client_lead_type_id = @clientleadtypeid;
        {CLIENTLEADSELECTBYID}
        ";
	}

	public static class HealthCareProfessionalTitleConstants
	{
		internal readonly static string HEALTHCAREPROFESSIONALTITLECOLUMNS =
		  @"
        health_care_professional_id As HealthCareProfessionalId,
        health_care_professional_title_id As HealthCareProfessionalTitleId,
        health_care_professional_title_description As HealthCareProfessionalTitleDescription,
        date_created As DateCreated,
        date_updated As DateUpdated,
        modified_by_user_id As ModifiedByUserId
      ";

		internal readonly static string HEALTHCAREPROFESSIONALTITLESBYHEALTHCAREPROFESSIONAL =
		  $@"
        select 
          {HEALTHCAREPROFESSIONALTITLECOLUMNS} 
        from
          health_care_professional_title
        where
          health_care_professional_id=@healthcareprofessionalid;
      ";

		internal readonly static string HEALTHCAREPROFESSIONALTITLEINSERT =
		  $@"
        insert into health_care_professional_title
        (
          health_care_professional_id,
          health_care_professional_title_description,
          date_created,
          date_updated,
          modified_by_user_id
        )
        values
        (
          @healthcareprofessionalid,
          @healthcareprofessionaltitledescription,
          now(),
          now(),
          @modifiedbyuserid
        );
        {HEALTHCAREPROFESSIONALTITLESBYHEALTHCAREPROFESSIONAL}
      ";

		internal readonly static string HEALTHCAREPROFESSIONALTITLEUPDATE =
		  $@"
        update health_care_professional_title
          set
            health_care_professional_title_description = @healthcareprofessionaltitledescription,
            date_updated = now(),
            modified_by_user_id = @modifiedbyuserid 
          where
            health_care_professional_id = @healthcareprofessionalid
            and
            health_care_professional_title_id = @healthcareprofessionaltitleid;
        {HEALTHCAREPROFESSIONALTITLESBYHEALTHCAREPROFESSIONAL}
      ";

		internal readonly static string HEALTHCAREPROFESSIONALTITLEDELETE =
		  $@"
        delete from health_care_professional_title
        where
          health_care_professional_id = @healthcareprofessionalid
          and
          health_care_professional_title_id = @healthcareprofessionaltitleid;
        {HEALTHCAREPROFESSIONALTITLESBYHEALTHCAREPROFESSIONAL}
      ";
	}

	public static class HealthCareProfessionalEducationConstants
	{
		internal readonly static string HEALTHCAREPROFESSIONALEDUCATIONCOLUMNS =
		  @"
        health_care_professional_id As HealthCareProfessionalId,
        health_care_professional_education_id As HealthCareProfessionalEducationId,
        health_care_professional_education_description As HealthCareProfessionalEducationDescription,
        health_care_professional_education_institution As HealthCareProfessionalEducationInstitution,
        date_created As DateCreated,
        date_updated As DateUpdated,
        modified_by_user_id As ModifiedByUserId
      ";

		internal readonly static string HEALTHCAREPROFESSIONALEDUCATIONBYHEALTHCAREPROFESSIONAL =
		  $@"
        select 
          {HEALTHCAREPROFESSIONALEDUCATIONCOLUMNS} 
        from
          health_care_professional_education
        where
          health_care_professional_id=@healthcareprofessionalid;
      ";

		internal readonly static string HEALTHCAREPROFESSIONALEDUCATIONINSERT =
		  $@"
        insert into health_care_professional_education
        (
          health_care_professional_id,
          health_care_professional_education_description,
          health_care_professional_education_institution,
          date_created,
          date_updated,
          modified_by_user_id
        )
        values
        (
          @healthcareprofessionalid,
          @healthcareprofessionaleducationdescription,
          @healthcareprofessionaleducationinstitution,
          now(),
          now(),
          @modifiedbyuserid
        );
        {HEALTHCAREPROFESSIONALEDUCATIONBYHEALTHCAREPROFESSIONAL}
      ";

		internal readonly static string HEALTHCAREPROFESSIONALEDUCATIONUPDATE =
		  $@"
        update health_care_professional_education
          set
            health_care_professional_education_description = @healthcareprofessionaleducationdescription,
            health_care_professional_education_institution = @healthcareprofessionaleducationinstitution,
            date_updated = now(),
            modified_by_user_id = @modifiedbyuserid 
          where
            health_care_professional_id = @healthcareprofessionalid
            and
            health_care_professional_education_id = @healthcareprofessionaleducationid;
        {HEALTHCAREPROFESSIONALEDUCATIONBYHEALTHCAREPROFESSIONAL}
      ";

		internal readonly static string HEALTHCAREPROFESSIONALEDUCATIONDELETE =
		  $@"
        delete from health_care_professional_education
        where
          health_care_professional_id = @healthcareprofessionalid
          and
          health_care_professional_education_id = @healthcareprofessionaleducationid;
        {HEALTHCAREPROFESSIONALEDUCATIONBYHEALTHCAREPROFESSIONAL}
      ";
	}

	public enum Events
	{
		SYSTEMERROR			 = 1,

		SAVECLIENTLEADERROR  = 1001,
		SAVECLIENTLEADINSERT = 1002,
		SAVECLIENTLEADUPDATE = 1003,

		SAVEHEALTHCAREPROFESSIONALERROR = 2001,
		SAVEHEALTHCAREPROFESSIONALINSERT = 2002,
		SAVEHEALTHCAREPROFESSIONALUPDATE = 2003,

		GETHEALTHCAREPROFESSIONALBYID = 2004,
		GETHEALTHCAREPROFESSIONALBYIDERROR = 2005,

		GETHEALTHCAREPROFESSIONALS = 2006,
		GETHEALTHCAREPROFESSIONALSERROR = 2007,

		SAVEHEALTHCAREPROFESSIONALTITLEINSERT = 3001,
		SAVEHEALTHCAREPROFESSIONALTITLEUPDATE = 3002,
		SAVEHEALTHCAREPROFESSIONALTITLEERROR = 3003,
		SAVEHEALTHCAREPROFESSIONALTITLEDUPLICATEERROR = 3004,

		DELETEHEALTHCAREPROFESSIONALTITLE = 3005,
		DELETEHEALTHCAREPROFESSIONALTITLERRROR = 3006,

		GETHEALTHCAREPROFESSIONALTITLESBYHEALTHCAREPROFESSIONAL = 3007,
		GETHEALTHCAREPROFESSIONALTITLESBYHEALTHCAREPROFESSIONALERROR = 3008,

		SAVEHEALTHCAREPROFESSIONALACCREDITATIONINSERT = 4001,
		SAVEHEALTHCAREPROFESSIONALACCREDITATIONUPDATE = 4002,
		SAVEHEALTHCAREPROFESSIONALACCREDITATIONERROR = 4003,
		SAVEHEALTHCAREPROFESSIONALACCREDITATIONDUPLICATEERROR = 4004,

		DELETEHEALTHCAREPROFESSIONALACCREDITATION = 4005,
		DELETEHEALTHCAREPROFESSIONALACCREDITATIONERROR = 4006,

		GETHEALTHCAREPROFESSIONALACCREDITATIONSBYHEALTHCAREPRFOESSIONAL = 4007,
		GETHEALTHCAREPROFESSIONALACCREDITATIONSBYHEALTHCAREPROFESSIONALERROR = 4008,

		SAVEHEALTHCAREPROFESSIONALEDUCATIONINSERT = 5001,
		SAVEHEALTHCAREPROFESSIONALEDUCATIONUPDATE = 5002,
		SAVEHEALTHCAREPROFESSIONALEDUCATIONERROR = 5003,
		SAVEHEALTHCAREPROFESSIONALEDUCATIONDUPLICATEERROR = 5004,

		DELETEHEALTHCAREPROFESSIONALEDUCATION = 5005,
		DELETEHEALTHCAREPROFESSIONALEDUCATIONERROR = 5006,

		GETHEALTHCAREPROFESSIONALEDUCATIONBYHEALTHCAREPRFOESSIONAL = 5007,
		GETHEALTHCAREPROFESSIONALEDUCATIONBYHEALTHCAREPROFESSIONALERROR = 5008
	}

	public static class EventLibrary
	{
		public static Dictionary<Events, string> EventDictionary
		{
			get
			{
				return new Dictionary<Events, string>
				{
					{
						Events.SYSTEMERROR,
						"There was an error in the system"
					},
					{
						Events.SAVECLIENTLEADERROR,
						"Error Saving Client Lead"
					},
					{
						Events.SAVECLIENTLEADINSERT,
						"Saving Client Lead - INSERT"
					},
					{
						Events.SAVECLIENTLEADUPDATE,
						"Saving Client Lead - UPDATE"
					},
					{
						Events.SAVEHEALTHCAREPROFESSIONALERROR,
						"Error Saving Health Care Professional"
					},
					{
						Events.SAVEHEALTHCAREPROFESSIONALINSERT,
						"Saving Health Care Professional - INSERT"
					},
					{
						Events.SAVEHEALTHCAREPROFESSIONALUPDATE,
						"Saving Health Care Professional - UPDATE"
					},
					{ 
						Events.GETHEALTHCAREPROFESSIONALBYID,
						"Retrieving Health Care Professional"
					},
					{
						Events.GETHEALTHCAREPROFESSIONALBYIDERROR,
						"Error Retrieving Health Care Professional"
					},
					{
						Events.GETHEALTHCAREPROFESSIONALS,
						"Retrieving Health Care Professionals"
					},
					{
						Events.GETHEALTHCAREPROFESSIONALSERROR,
						"Error Retrieving Health Care Professionals"
					},
					{
						Events.SAVEHEALTHCAREPROFESSIONALTITLEINSERT,
						"Saving Health Care Professional Title - INSERT"
					},
					{
						Events.SAVEHEALTHCAREPROFESSIONALTITLEUPDATE,
						"Saving Health Care Professional Title - UPDATE"
					},
					{
						Events.SAVEHEALTHCAREPROFESSIONALTITLEERROR,
						"Error Saving Health Care Professional Title"
					},
					{
						Events.SAVEHEALTHCAREPROFESSIONALTITLEDUPLICATEERROR,
						"Error Duplicate Health Care Professional Title"
					},
					{
						Events.DELETEHEALTHCAREPROFESSIONALTITLE,
						"Deleting Health Care Professional Title"
					},
					{
						Events.DELETEHEALTHCAREPROFESSIONALTITLERRROR,
						"Error Deleting Health Care Professional Title"
					},
					{
						Events.GETHEALTHCAREPROFESSIONALTITLESBYHEALTHCAREPROFESSIONAL,
						"Retrieving Health Care Professional Titles by Health Care Professional"
					},
					{
						Events.GETHEALTHCAREPROFESSIONALTITLESBYHEALTHCAREPROFESSIONALERROR,
						"Error Retrieving Health Care Professional Titles by Health Care Professional"
					},
					{
						Events.SAVEHEALTHCAREPROFESSIONALACCREDITATIONINSERT,
						"Saving Health Care Professional Accreditation - INSERT"
					},
					{
						Events.SAVEHEALTHCAREPROFESSIONALACCREDITATIONUPDATE,
						"Saving Health Care Professional Accreditation - UPDATE"
					},
					{
						Events.SAVEHEALTHCAREPROFESSIONALACCREDITATIONERROR,
						"Error Saving Health Care Professional Accreditation"
					},
					{
						Events.SAVEHEALTHCAREPROFESSIONALACCREDITATIONDUPLICATEERROR,
						"Error Duplicate Health Care Professional Accreditation"
					},
					{
						Events.DELETEHEALTHCAREPROFESSIONALACCREDITATION,
						"Deleting Health Care Professional Accreditation"
					},
					{
						Events.DELETEHEALTHCAREPROFESSIONALACCREDITATIONERROR,
						"Error Deleting Health Care Professional Accreditation"
					},
					{
						Events.GETHEALTHCAREPROFESSIONALACCREDITATIONSBYHEALTHCAREPRFOESSIONAL,
						"Retrieving Health Care Professional Accreditations By Health Care Professional"
					},
					{
						Events.GETHEALTHCAREPROFESSIONALACCREDITATIONSBYHEALTHCAREPROFESSIONALERROR,
						"Error Retrieving Health Care Professional Accreditations By Health Care Professional"
					},
					{
						Events.SAVEHEALTHCAREPROFESSIONALEDUCATIONINSERT,
						"Saving Health Care Professional Education - INSERT"
					},
					{
						Events.SAVEHEALTHCAREPROFESSIONALEDUCATIONUPDATE,
						"Saving Health Care Professional Education - UPDATE"
					},
					{
						Events.SAVEHEALTHCAREPROFESSIONALEDUCATIONERROR,
						"Error Saving Health Care Professional Education"
					},
					{
						Events.SAVEHEALTHCAREPROFESSIONALEDUCATIONDUPLICATEERROR,
						"Error Duplicate Health Care Professional Education"
					},
					{
						Events.DELETEHEALTHCAREPROFESSIONALEDUCATION,
						"Deleting Health Care Professional Education"
					},
					{
						Events.DELETEHEALTHCAREPROFESSIONALEDUCATIONERROR,
						"Error Deleting Health Care Professional Education"
					},
					{
						Events.GETHEALTHCAREPROFESSIONALEDUCATIONBYHEALTHCAREPRFOESSIONAL,
						"Retrieving Health Care Professional Education By Health Care Professional"
					},
					{
						Events.GETHEALTHCAREPROFESSIONALEDUCATIONBYHEALTHCAREPROFESSIONALERROR,
						"Error Retrieving Health Care Professional Education By Health Care Professional"
					}
				};
			}
		}
	}
}
