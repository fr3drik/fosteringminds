﻿using FosteringMinds.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FosteringMinds.Repositories
{
    public interface IPersonRepository<T> : IBaseRepository<T> where T : Person
    {
		Task<Person> SavePerson(Person person);
    }
}
