﻿using DataAccess;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FosteringMinds.Repositories
{
    public interface IBaseRepository<T>
    {
		IDbManager DbManager { get; }
		IList<ValidationResult> ValidationResults { get; }
		IConfiguration Configuration { get; }
	}
}
