﻿using FosteringMinds.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FosteringMinds.Repositories
{
    public interface IAccountRepository<T> : IBaseRepository<T> where T : Account
    {
		Task<Account> SaveAccount(Account account);
    }
}
