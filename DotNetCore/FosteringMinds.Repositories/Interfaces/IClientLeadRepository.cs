﻿using FosteringMinds.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace FosteringMinds.Repositories
{
	public interface IClientLeadRepository<T> : IBaseRepository<T> where T: ClientLead
	{
		ClientLead SaveClientLead(ClientLead clientLead);
		ClientLead GetClientLeadById(int healthCareProfessionalId, int clientLeadId);
		IList<ClientLead> GetClientLeads();
		IList<ClientLead> GetClientLeadsByHealthCareProfessional(int healthCareProfessionalId);
		ClientLeadType SaveClientLeadType(ClientLeadType clientLeadType);
		ClientLeadType GetClientLeadTypeById(int clientLeadTypeId);
		ClientLeadType GetClientLeadTypeByDescription(string clientLeadTypeDescription);
		IList<ClientLeadType> GetClientLeadTypesByDescription(string clientLeadTypeDescription);
		IList<ClientLeadType> GetClientLeadTypes();
		IList<ClientLeadType> GetClientLeadTypesByHealthCareProfessional(int healthCareProfessionalId);
	}
}
