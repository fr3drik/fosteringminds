﻿using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using System;
using System.Data;

namespace DataAccess
{
	public class DbManager : IDbManager, IDisposable
	{
		private IDbConnection dbConnection { get; set; }

		public IDbConnection DbConnection
		{
			get
			{
				if (dbConnection.State == ConnectionState.Closed)
					dbConnection.Open();

				return dbConnection;
			}
		}

		public DbManager()
		{

		}
		public DbManager(string connectionString)
		{
			dbConnection = new MySqlConnection(connectionString);
		}

		public void Dispose()
		{
			if (dbConnection == null) return;
			if (dbConnection.State == ConnectionState.Open)
			{
				dbConnection.Close();
				dbConnection.Dispose();
			}
			dbConnection = null;
		}
	}
}

