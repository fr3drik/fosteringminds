﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DataAccess
{
    public interface IDbManager : IDisposable
    {
		IDbConnection DbConnection { get; }
    }
}
