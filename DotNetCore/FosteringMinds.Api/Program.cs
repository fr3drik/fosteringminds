﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using System;
using System.IO;

namespace FosteringMinds.Api
{
	public class Program
    {
        public static void Main(string[] args)
        {
			var configuration = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
				.AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", optional: true, reloadOnChange: true)
				.AddEnvironmentVariables()
				.Build();

			Log.Logger = new LoggerConfiguration()
							.ReadFrom.Configuration(configuration)
							.CreateLogger();

			try
			{
				Log.Information("Initialising");

				var host = new WebHostBuilder()
					.UseKestrel()
					.UseStartup<Startup>()
					.UseConfiguration(configuration)
					.UseSerilog()
					.Build();

				host.Run();
			}
			catch (Exception ex)
			{
				Log.Fatal(ex, "Host terminated unexpectedly");
			}
			finally
			{
				Log.CloseAndFlush();
			}
		}
    }
}
