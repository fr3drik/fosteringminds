﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using FosteringMinds.Repositories;
using FosteringMinds.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FosteringMinds.Api.Controllers
{
    [Route("api/[controller]")]
    public class HealthCareProfessionalController : Controller
    {
		ILogger<HealthCareProfessionalController> logger;
		IHealthCareProfessionalRepository<HealthCareProfessional> healthCareProfessionalRepository;

		public HealthCareProfessionalController(IHealthCareProfessionalRepository<HealthCareProfessional> healthCareProfessionalRepository, ILogger<HealthCareProfessionalController> logger)
		{
			this.logger = logger;
			this.healthCareProfessionalRepository = healthCareProfessionalRepository;
		}

        [HttpGet]
        public IEnumerable<HealthCareProfessional> Get()
        {
            return healthCareProfessionalRepository.GetHealthCareProfessionals();
        }

        [HttpGet("{id}")]
        public HealthCareProfessional Get(int id)
        {
			var healthCareProfessional = healthCareProfessionalRepository.GetHealthCareProfessionalById(id);
			healthCareProfessional.ProfessionalTitles = healthCareProfessionalRepository.GetHealthCareProfessionalTitles(id);
			healthCareProfessional.Education = healthCareProfessionalRepository.GetHealthCareProfessionalEducation(id);
			healthCareProfessional.Accreditations = healthCareProfessionalRepository.GetHealthCareProfessionalAccreditations(id);
            return healthCareProfessional;
        }

        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
