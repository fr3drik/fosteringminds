using DataAccess;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace DataAccess.Tests
{
    [TestClass]
    public class DbManagerTests
    {
		IDbManager db;
		[TestInitialize]
		public void Initialise()
		{
			string connectionString = "Server=localhost;Database=fosteringminds;Uid=fosteringminds;Pwd=Password@123;SslMode=none";
			db = new DbManager(connectionString);
		}
		[TestMethod]
		public void Integration_DbManager_ConnectionTest()
		{
			using (db)
			{
				Assert.IsTrue(db.DbConnection.State == System.Data.ConnectionState.Open);
			}
		}
	}
}
