﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FosteringMinds.Models
{
	public class Person : BaseEntity
	{
		public string PersonKey { get; set; }
		public string Names { get; set; }
		public string LastName { get; set; }
		public string IdentityNumber { get; set; }
		public PersonType PersonType { get; set; }
	}

	public enum PersonType
	{
		Learner,
		Parent,
		Teacher,
		Professional,
		Lead,
		Client
	}
}
