﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FosteringMinds.Models
{
	public class HealthCareProfessionalAccreditation : BaseEntity
	{
		public int HealthCareProfessionalId { get; set; }
		public int HealthCareProfessionalAccreditationId { get; set; }
		[Required(ErrorMessage = "Accreditation is required")]
		public string HealthCareProfessionalAccreditationTitle { get; set; }
		[Required(ErrorMessage = "Accreditation organisation is required")]
		public string HealthCareProfessionalAccreditationOrganisation { get; set; }
	}
}
