﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FosteringMinds.Models
{
    public class Account : BaseEntity
    {
		public string AccountKey { get; set; }
		public string AccountReference { get; set; }
    }
}
