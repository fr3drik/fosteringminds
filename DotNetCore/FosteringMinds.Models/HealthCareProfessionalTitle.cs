﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FosteringMinds.Models
{
	public class HealthCareProfessionalTitle : BaseEntity
	{
		public int HealthCareProfessionalId { get; set; }
		public int HealthCareProfessionalTitleId { get; set; }
		[Required]
		[MinLength(2)]
		public string HealthCareProfessionalTitleDescription { get; set; }
	}
}
