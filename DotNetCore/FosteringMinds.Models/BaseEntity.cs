﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FosteringMinds.Models
{
	public class BaseEntity
	{
		public DateTime DateCreated { get; set; }
		public DateTime DateUpdated { get; set; }
		public string ModifiedByUserId { get; set; }
	}
}
