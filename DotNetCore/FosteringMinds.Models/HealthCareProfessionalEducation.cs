﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FosteringMinds.Models
{
    public class HealthCareProfessionalEducation : BaseEntity
    {
		public int HealthCareProfessionalId { get; set; }
		public int HealthCareProfessionalEducationId { get; set; }
		public string HealthCareProfessionalEducationDescription { get; set; }
		public string HealthCareProfessionalEducationInstitution { get; set; }
    }
}
