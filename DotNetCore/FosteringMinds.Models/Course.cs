﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FosteringMinds.Models
{
    public class Course : BaseEntity
    {
		public string CourseKey { get; set; }
		public string CourseTitle { get; set; }
    }
}
