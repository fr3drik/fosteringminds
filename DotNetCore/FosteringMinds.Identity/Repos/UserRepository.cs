﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Identity;
using Dapper;

namespace FosteringMinds.Identity.Repos
{
    public class UserRepository
    {
		private IDbManager dbManager;

		public UserRepository(IDbManager dbManager)
		{
			this.dbManager = dbManager;
		}

		public Task<IdentityResult> CreateAsync(ApplicationUser user, CancellationToken cancellationToken)
		{
			const string command = "INSERT INTO dbo.Users " +
								   "VALUES (@Id, @FirstName, @LastName, @UserName, @Email, @EmailConfirmed, " +
										   "@PasswordHash, @PhoneNumber, @PhoneNumberConfirmed, @PhotoUrl, @Address, @ConcurrencyStamp, @SecurityStamp, " +
										   "@RegistrationDate, @LastLoginDate, @LockoutEnabled, @LockoutEndDateTimeUtc, @TwoFactorEnabled, @AccessFailedCount);";

			int rowsInserted = Task.Run(() => dbManager.DbConnection.ExecuteAsync(command, new
			{
				user.Id,
				user.FirstName,
				user.LastName,
				user.UserName,
				user.NormalizedUserName,
				user.Email,
				user.NormalizedEmail,
				user.EmailConfirmed,
				user.PasswordHash,
				user.PhoneNumber,
				user.PhoneNumberConfirmed,
				user.PhotoUrl,
				user.Address,
				user.ConcurrencyStamp,
				user.SecurityStamp,
				user.RegistrationDate,
				user.LastLoginDate,
				user.LockoutEnabled,
				user.LockoutEndDateTimeUtc,
				user.TwoFactorEnabled,
				user.AccessFailedCount
			}), cancellationToken).Result;

			return Task.FromResult(rowsInserted.Equals(1) ? IdentityResult.Success : IdentityResult.Failed(new IdentityError
			{
				Code = string.Empty,
				Description = $"The user with email {user.Email} could not be inserted in the dbo.Users table."
			}));
		}
	}
}
