﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FosteringMinds.Identity
{
	public class UserClaim
	{
		public Guid Id { get; set; }
		public Guid UserId { get; set; }
		public string ClaimType { get; set; }
		public string ClaimValue { get; set; }
	}
}
