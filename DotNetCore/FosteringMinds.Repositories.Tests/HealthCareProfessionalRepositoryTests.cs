using DataAccess;
using FosteringMinds.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace FosteringMinds.Repositories.Tests
{
    [TestClass]
    public class HealthCareProfessionalRepositoryTests
    {
		IDbManager db;
		IHealthCareProfessionalRepository<HealthCareProfessional> healthCareProfessionalRepository;
		FakeLogger<HealthCareProfessionalRepository<HealthCareProfessional>> healthCareProfessionalLogger;
		IConfiguration Configuration;
		int healthCareProfessionalId = 0;

		[TestInitialize]
		public void Initialise()
		{
			var builder = new ConfigurationBuilder()
					.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

			Configuration = builder.Build();

			healthCareProfessionalId = int.Parse(Configuration["HealthCareProfessionalId"]);

			healthCareProfessionalLogger = new FakeLogger<HealthCareProfessionalRepository<HealthCareProfessional>>();
			healthCareProfessionalRepository = new HealthCareProfessionalRepository<HealthCareProfessional>(healthCareProfessionalLogger, Configuration);
		}
        [TestMethod]
        public void SaveHealthCareProfessionalTest_Insert()
        {
			var healthCareProfessional = new HealthCareProfessional();
			var datestamp = DateTime.Now.ToLongTimeString();
			var healthCareProfessionalFirstName = "Franz - Inserted XYZ - " + datestamp;
			var healthCareProfessionalLastName = "Ferdinand - Inserted XYZ - " + datestamp;
			var healthCareProfessionalTitle = "Mr - Inserted - " + datestamp;
			var healthCareProfessionalBio = "Test Bio - Inserted - " + datestamp;
			var healthCareProfessionalUrl = "http://www.something.net - Inserted - " + datestamp;
			var modifiedByUserId = "ac7da837-a4b5-4e17-a925-7a0d71ceddbc";

			healthCareProfessional.HealthCareProfessionalBio = healthCareProfessionalBio;
			healthCareProfessional.HealthCareProfessionalFirstName = healthCareProfessionalFirstName;
			healthCareProfessional.HealthCareProfessionalLastName = healthCareProfessionalLastName;
			healthCareProfessional.HealthCareProfessionalTitle = healthCareProfessionalTitle;
			healthCareProfessional.HealthCareProfessionalUrl = healthCareProfessionalUrl;
			healthCareProfessional.ModifiedByUserId = modifiedByUserId;

			var c = healthCareProfessionalRepository.SaveHealthCareProfessional(healthCareProfessional);

			Assert.IsNotNull(c);
			Assert.AreEqual(healthCareProfessionalLogger.EventId.Id, (int)Events.SAVEHEALTHCAREPROFESSIONALINSERT);
		}

		[TestMethod]
		public void SaveHealthCareProfessionalTest_Update()
		{
			var healthCareProfessional = new HealthCareProfessional();
			
			var datestamp = DateTime.Now.ToLongTimeString();
			var healthCareProfessionalFirstName = "Franz - Updated XYZ - " + datestamp;
			var healthCareProfessionalLastName = "Ferdinand - Updated XYZ - " + datestamp;
			var healthCareProfessionalTitle = "Mr - Updated - " + datestamp;
			var healthCareProfessionalBio = "Test Bio - Updated - " + datestamp;
			var healthCareProfessionalUrl = "http://www.something.net - Updated - " + datestamp;
			var modifiedByUserId = "ac7da837-a4b5-4e17-a925-7a0d71ceddbc";

			healthCareProfessional.HealthCareProfessionalId = healthCareProfessionalId;
			healthCareProfessional.HealthCareProfessionalBio = healthCareProfessionalBio;
			healthCareProfessional.HealthCareProfessionalFirstName = healthCareProfessionalFirstName;
			healthCareProfessional.HealthCareProfessionalLastName = healthCareProfessionalLastName;
			healthCareProfessional.HealthCareProfessionalTitle = healthCareProfessionalTitle;
			healthCareProfessional.HealthCareProfessionalUrl = healthCareProfessionalUrl;
			healthCareProfessional.ModifiedByUserId = modifiedByUserId;

			var c = healthCareProfessionalRepository.SaveHealthCareProfessional(healthCareProfessional);

			Assert.IsNotNull(c);
			Assert.AreEqual(healthCareProfessionalLogger.EventId.Id, (int)Events.SAVEHEALTHCAREPROFESSIONALUPDATE);
		}

		[TestMethod]
		public void GetHealthCareProfessionalByIdTest()
		{
			var c = healthCareProfessionalRepository.GetHealthCareProfessionalById(healthCareProfessionalId);

			Assert.IsNotNull(c);
			Assert.AreEqual(healthCareProfessionalLogger.EventId.Id, (int)Events.GETHEALTHCAREPROFESSIONALBYID);
		}

		[TestMethod]
		public void GetHealthCareProfessionalsTest()
		{
			var c = healthCareProfessionalRepository.GetHealthCareProfessionals();

			Assert.IsNotNull(c);
			Assert.AreEqual(healthCareProfessionalLogger.EventId.Id, (int)Events.GETHEALTHCAREPROFESSIONALS);
		}

		[TestMethod]
		public void SaveHealthCareProfessionalAccreditationTestInsert()
		{
			var healthCareProfessional = healthCareProfessionalRepository.GetHealthCareProfessionalById(healthCareProfessionalId);
			var healthCareProfessionalAccreditation = new HealthCareProfessionalAccreditation { HealthCareProfessionalId = healthCareProfessional.HealthCareProfessionalId, DateCreated = DateTime.Now, DateUpdated = DateTime.Now, HealthCareProfessionalAccreditationOrganisation = "Test Description", HealthCareProfessionalAccreditationTitle = "Test Title" + DateTime.Now.Second, ModifiedByUserId = Guid.NewGuid().ToString() };
			var c = healthCareProfessionalRepository.SaveHealthCareProfessionalAccreditation(healthCareProfessionalAccreditation);

			Assert.IsNotNull(c);
			Assert.AreEqual(healthCareProfessionalLogger.EventId.Id, (int)Events.SAVEHEALTHCAREPROFESSIONALACCREDITATIONINSERT);
		}

		[TestMethod]
		public void SaveHealthCareProfessionalAccreditationTestDuplicateInsert()
		{
			var healthCareProfessional = healthCareProfessionalRepository.GetHealthCareProfessionalById(healthCareProfessionalId);
			var healthCareProfessionalAccreditation = new HealthCareProfessionalAccreditation { HealthCareProfessionalId = healthCareProfessional.HealthCareProfessionalId, DateCreated = DateTime.Now, DateUpdated = DateTime.Now, HealthCareProfessionalAccreditationOrganisation = "Test Description", HealthCareProfessionalAccreditationTitle = "Test Title", ModifiedByUserId = Guid.NewGuid().ToString() };
			var c = healthCareProfessionalRepository.SaveHealthCareProfessionalAccreditation(healthCareProfessionalAccreditation);

			Assert.IsNotNull(c);
			Assert.AreEqual(healthCareProfessionalLogger.EventId.Id, (int)Events.SAVEHEALTHCAREPROFESSIONALACCREDITATIONDUPLICATEERROR);
		}

		[TestMethod]
		public void GetHealthCareProfessionalAccreditationsTest()
		{
			var c = healthCareProfessionalRepository.GetHealthCareProfessionalAccreditations(healthCareProfessionalId);

			Assert.IsNotNull(c);
			Assert.AreEqual(healthCareProfessionalLogger.EventId.Id, (int)Events.GETHEALTHCAREPROFESSIONALACCREDITATIONSBYHEALTHCAREPRFOESSIONAL);
		}

		[TestMethod]
		public void SaveHealthCareProfessionalAccreditationTestUpdate()
		{
			var healthCareProfessional = healthCareProfessionalRepository.GetHealthCareProfessionalById(healthCareProfessionalId);
			var accreditations = healthCareProfessionalRepository.GetHealthCareProfessionalAccreditations(healthCareProfessional.HealthCareProfessionalId);
			var healthCareProfessionalAccreditation = accreditations.First();
			healthCareProfessionalAccreditation.HealthCareProfessionalAccreditationTitle = "Updated Title";
			var c = healthCareProfessionalRepository.SaveHealthCareProfessionalAccreditation(healthCareProfessionalAccreditation);
			var e = c.Where(h => h.HealthCareProfessionalAccreditationId == healthCareProfessionalAccreditation.HealthCareProfessionalAccreditationId).First();

			Assert.IsNotNull(e);
			Assert.AreEqual(e.HealthCareProfessionalAccreditationId, healthCareProfessionalAccreditation.HealthCareProfessionalAccreditationId);
			Assert.AreEqual(healthCareProfessionalLogger.EventId.Id, (int)Events.SAVEHEALTHCAREPROFESSIONALACCREDITATIONUPDATE);
		}

		[TestMethod]
		public void DeleteHealthCareProfessionalAccreditationTest()
		{
			var healthCareProfessional = healthCareProfessionalRepository.GetHealthCareProfessionalById(healthCareProfessionalId);
			var accreditations = healthCareProfessionalRepository.GetHealthCareProfessionalAccreditations(healthCareProfessional.HealthCareProfessionalId);
			var healthCareProfessionalAccreditation = accreditations.First();
			var c = healthCareProfessionalRepository.DeleteHealthCareProfessionalAccreditation(healthCareProfessionalAccreditation);			

			Assert.IsNotNull(c);
			Assert.IsTrue(c.Where(a => a.HealthCareProfessionalAccreditationId == healthCareProfessionalAccreditation.HealthCareProfessionalAccreditationId).Count() == 0);
			Assert.AreEqual(healthCareProfessionalLogger.EventId.Id, (int)Events.DELETEHEALTHCAREPROFESSIONALACCREDITATION);
		}

		[TestMethod]
		public void SaveHealthCareProfessionalTitleTestInsert()
		{
			var healthCareProfessional = healthCareProfessionalRepository.GetHealthCareProfessionalById(healthCareProfessionalId);
			var professionalTitle = new HealthCareProfessionalTitle { DateCreated = DateTime.Now, DateUpdated = DateTime.Now, HealthCareProfessionalId = healthCareProfessional.HealthCareProfessionalId, HealthCareProfessionalTitleDescription = "Test Description" + DateTime.Now.Second, ModifiedByUserId = Guid.NewGuid().ToString() };

			var c = healthCareProfessionalRepository.SaveHealthCareProfessionalTitle(professionalTitle);

			Assert.IsNotNull(c);
			Assert.AreEqual(healthCareProfessionalLogger.EventId.Id, (int)Events.SAVEHEALTHCAREPROFESSIONALTITLEINSERT);
		}

		[TestMethod]
		public void GetHealthCareProfessionalTitlesTest()
		{
			var healthCareProfessional = healthCareProfessionalRepository.GetHealthCareProfessionalById(healthCareProfessionalId);
			var c = healthCareProfessionalRepository.GetHealthCareProfessionalTitles(healthCareProfessional.HealthCareProfessionalId);

			Assert.IsNotNull(c);
			Assert.AreEqual(healthCareProfessionalLogger.EventId.Id, (int)Events.GETHEALTHCAREPROFESSIONALTITLESBYHEALTHCAREPROFESSIONAL);
		}

		[TestMethod]
		public void SaveHealthCareProfessionalTitleTestUpdate()
		{
			var healthCareProfessional = healthCareProfessionalRepository.GetHealthCareProfessionalById(healthCareProfessionalId);
			var professionalTitles = healthCareProfessionalRepository.GetHealthCareProfessionalTitles(healthCareProfessional.HealthCareProfessionalId);
			var professionalTitle = professionalTitles.First();
			professionalTitle.HealthCareProfessionalTitleDescription = "Title Description Update";

			var c = healthCareProfessionalRepository.SaveHealthCareProfessionalTitle(professionalTitle);
			var e = c.Where(f => f.HealthCareProfessionalTitleId == professionalTitle.HealthCareProfessionalTitleId).First();

			Assert.IsNotNull(c);
			Assert.AreEqual(e.HealthCareProfessionalTitleId, professionalTitle.HealthCareProfessionalTitleId);
			Assert.AreEqual(healthCareProfessionalLogger.EventId.Id, (int)Events.SAVEHEALTHCAREPROFESSIONALTITLEUPDATE);
		}

		[TestMethod]
		public void DeleteHealthCareProfessionalTitleTest()
		{
			var healthCareProfessional = healthCareProfessionalRepository.GetHealthCareProfessionalById(healthCareProfessionalId);
			var professionalTitles = healthCareProfessionalRepository.GetHealthCareProfessionalTitles(healthCareProfessional.HealthCareProfessionalId);
			var professionalTitle = professionalTitles.First();
			var c = healthCareProfessionalRepository.DeleteHealthCareProfessionalTitle(professionalTitle);

			Assert.IsNotNull(c);
			Assert.IsTrue(c.Where(a => a.HealthCareProfessionalTitleId == professionalTitle.HealthCareProfessionalTitleId).Count() == 0);
			Assert.AreEqual(healthCareProfessionalLogger.EventId.Id, (int)Events.DELETEHEALTHCAREPROFESSIONALTITLE);
		}

		[TestMethod]
		public void SaveHealthCareProfessionalEducationTestInsert()
		{
			var healthCareProfessional = healthCareProfessionalRepository.GetHealthCareProfessionalById(healthCareProfessionalId);
			var education = new HealthCareProfessionalEducation { DateCreated = DateTime.Now, DateUpdated = DateTime.Now, HealthCareProfessionalId = healthCareProfessional.HealthCareProfessionalId, HealthCareProfessionalEducationDescription = "Test Description" + DateTime.Now.Second, HealthCareProfessionalEducationInstitution = "Institution " + DateTime.Now.Second,  ModifiedByUserId = Guid.NewGuid().ToString() };

			var c = healthCareProfessionalRepository.SaveHealthCareProfessionalEducation(education);

			Assert.IsNotNull(c);
			Assert.AreEqual(healthCareProfessionalLogger.EventId.Id, (int)Events.SAVEHEALTHCAREPROFESSIONALEDUCATIONINSERT);
		}

		[TestMethod]
		public void GetHealthCareProfessionalEducationTest()
		{
			var healthCareProfessional = healthCareProfessionalRepository.GetHealthCareProfessionalById(healthCareProfessionalId);
			var c = healthCareProfessionalRepository.GetHealthCareProfessionalEducation(healthCareProfessional.HealthCareProfessionalId);

			Assert.IsNotNull(c);
			Assert.AreEqual(healthCareProfessionalLogger.EventId.Id, (int)Events.GETHEALTHCAREPROFESSIONALEDUCATIONBYHEALTHCAREPRFOESSIONAL);
		}

		[TestMethod]
		public void SaveHealthCareProfessionalEducationTestUpdate()
		{
			var healthCareProfessional = healthCareProfessionalRepository.GetHealthCareProfessionalById(healthCareProfessionalId);
			var educationList = healthCareProfessionalRepository.GetHealthCareProfessionalEducation(healthCareProfessional.HealthCareProfessionalId);
			var education = educationList.First();
			education.HealthCareProfessionalEducationDescription = "Education Description Update";
			education.HealthCareProfessionalEducationInstitution = "Education Institution Update";

			var c = healthCareProfessionalRepository.SaveHealthCareProfessionalEducation(education);
			var e = c.Where(f => f.HealthCareProfessionalEducationId == education.HealthCareProfessionalEducationId).First();

			Assert.IsNotNull(c);
			Assert.AreEqual(e.HealthCareProfessionalEducationId, education.HealthCareProfessionalEducationId);
			Assert.AreEqual(healthCareProfessionalLogger.EventId.Id, (int)Events.SAVEHEALTHCAREPROFESSIONALEDUCATIONUPDATE);
		}

		[TestMethod]
		public void DeleteHealthCareProfessionalEducationTest()
		{
			var healthCareProfessional = healthCareProfessionalRepository.GetHealthCareProfessionalById(healthCareProfessionalId);
			var educationList = healthCareProfessionalRepository.GetHealthCareProfessionalEducation(healthCareProfessional.HealthCareProfessionalId);
			var education = educationList.First();
			var c = healthCareProfessionalRepository.DeleteHealthCareProfessionalEducation(education);

			Assert.IsNotNull(c);
			Assert.IsTrue(c.Where(a => a.HealthCareProfessionalEducationId == education.HealthCareProfessionalEducationId).Count() == 0);
			Assert.AreEqual(healthCareProfessionalLogger.EventId.Id, (int)Events.DELETEHEALTHCAREPROFESSIONALEDUCATION);
		}
	}
}
