﻿using DataAccess;
using FosteringMinds.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace FosteringMinds.Repositories.Tests
{
	[TestClass]
    public class PersonRepositoryTests
    {
		IDbManager db;
		IPersonRepository<Person> personRepository;
		FakeLogger<PersonRepository<Person>> personRepositoryLogger;
		IConfiguration Configuration;

		[TestInitialize]
		public void Initialise()
		{
			var builder = new ConfigurationBuilder()
					.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

			Configuration = builder.Build();

			personRepositoryLogger = new FakeLogger<PersonRepository<Person>>();
			personRepository = new PersonRepository<Person>(personRepositoryLogger, Configuration);
		}
		[TestMethod]
		public void SavePersonTest_Insert()
		{
			var person = new Person { PersonKey = Guid.NewGuid().ToString(), IdentityNumber = "xxx", LastName = "Smith", Names = "Ron", PersonType = PersonType.Learner, DateCreated = DateTime.Now, DateUpdated = DateTime.Now, ModifiedByUserId = string.Empty };
			var c = personRepository.SavePerson(person);

			Assert.IsTrue(c.Status == System.Threading.Tasks.TaskStatus.RanToCompletion);
			Assert.IsNotNull(c.Result.PersonKey);
			Assert.IsNotNull(c.Result.LastName);
			Assert.IsNotNull(c.Result.Names);
			Assert.IsNotNull(c.Result.IdentityNumber);
			Assert.IsTrue(c.Result.PersonType == PersonType.Learner);
		}
	}
}
