﻿using DataAccess;
using FosteringMinds.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace FosteringMinds.Repositories.Tests
{
	[TestClass]
    public class AccountRepositoryTests
    {
		IDbManager db;
		IAccountRepository<Account> accountRepository;
		FakeLogger<AccountRepository<Account>> accountRepositoryLogger;
		IConfiguration Configuration;

		[TestInitialize]
		public void Initialise()
		{
			var builder = new ConfigurationBuilder()
					.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

			Configuration = builder.Build();

			accountRepositoryLogger = new FakeLogger<AccountRepository<Account>>();
			accountRepository = new AccountRepository<Account>(accountRepositoryLogger, Configuration);
		}
		[TestMethod]
		public void SaveAccountTest_Insert()
		{
			var account = new Account { AccountKey = Guid.NewGuid().ToString(), AccountReference = "xxxxxx", DateCreated = DateTime.Now, DateUpdated = DateTime.Now, ModifiedByUserId = string.Empty };
			var c = accountRepository.SaveAccount(account);

			Assert.IsTrue(c.Status == System.Threading.Tasks.TaskStatus.RanToCompletion);
			Assert.IsNotNull(c.Result.AccountKey);
		}

		[TestMethod]
		public void SaveAccountTest_DuplicateReference_Insert()
		{
			var account = new Account { AccountKey = Guid.NewGuid().ToString(), AccountReference = "12345678", DateCreated = DateTime.Now, DateUpdated = DateTime.Now, ModifiedByUserId = string.Empty };
			var c = accountRepository.SaveAccount(account);

			Assert.IsTrue(c.Status == System.Threading.Tasks.TaskStatus.Faulted);
		}
	}
}
