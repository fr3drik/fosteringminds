﻿using System;
using System.Data;

namespace Corsair
{
  public interface IDbManager : IDisposable
  {
    IDbConnection Connection { get; }
  }
}