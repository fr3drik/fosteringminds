﻿using System;
using System.Configuration;
using System.Data;
using Corsair;
using MySql.Data.MySqlClient;

namespace DbManager.MySql
{
  public class DbManager : IDbManager, IDisposable
  {
    private IDbConnection Conn { get; set; }

    public IDbConnection Connection
    {
      get
      {
        if (Conn.State == ConnectionState.Closed)
          Conn.Open();

        return Conn;
      }
    }
    public DbManager()
    {

    }
    public DbManager(string connectionStringName)
    {
      var connectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
      Conn = new MySqlConnection(connectionString);
    }

    public void Dispose()
    {
      if (Conn == null) return;
      if (Conn.State == ConnectionState.Open)
      {
        Conn.Close();
        Conn.Dispose();
      }
      Conn = null;
    }
  }
}
