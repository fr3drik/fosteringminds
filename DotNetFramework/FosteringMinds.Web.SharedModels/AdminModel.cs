﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using FosteringMinds.Models;
using FosteringMinds.Repositories;
using FosteringMinds.Repositories.Interfaces;
using Microsoft.AspNet.Identity;

namespace FosteringMinds.Web.SharedModels
{
  public class AdminModel
  {
    public AdminModel()
    {

    }

    public AdminModel(string viewName, ControllerContext controllerContext, IHealthCareProfessionalRepository healthCareProfessionalRepository)
    {
      this.viewName = viewName;
      this.healthCareProfessionalRepository = healthCareProfessionalRepository;
      SetModel();
      this.viewHtml = HtmlHelperExtensions.RenderPartialToString("partials/_" + viewName + "Partial", this, controllerContext);
    }
    public AdminModel(string viewName, int healthProfessionalId, ControllerContext controllerContext, IHealthCareProfessionalRepository healthCareProfessionalRepository)
    {
      this.viewName = viewName;
      this.healthCareProfessionalRepository = healthCareProfessionalRepository;
      this.healthCareProfessionalId = healthProfessionalId;
      SetModel();
      this.viewHtml = HtmlHelperExtensions.RenderPartialToString("partials/_" + viewName + "Partial", this, controllerContext);
    }
    public AdminModel(string viewName, string healthCareProfessionalUrl, ControllerContext controllerContext, IHealthCareProfessionalRepository healthCareProfessionalRepository)
    {
      this.viewName = viewName;
      this.healthCareProfessionalRepository = healthCareProfessionalRepository;
      this.healthCareProfessionalUrl = healthCareProfessionalUrl;
      SetModel();
      this.viewHtml = HtmlHelperExtensions.RenderPartialToString("partials/_" + viewName + "Partial", this, controllerContext);
    }
    public AdminModel(string viewName, int healthProfessionalId, ControllerContext controllerContext, IHealthCareProfessionalRepository healthCareProfessionalRepository, IClientLeadRepository clientLeadRepository)
    {
      this.viewName = viewName;
      this.healthCareProfessionalRepository = healthCareProfessionalRepository;
      this.clientLeadRepository = clientLeadRepository;
      this.healthCareProfessionalId = healthProfessionalId;
      SetModel();
      this.viewHtml = HtmlHelperExtensions.RenderPartialToString("partials/_" + viewName + "Partial", this, controllerContext);
    }
    public AdminModel(string viewName, int healthProfessionalId, int clientLeadId, ControllerContext controllerContext, IHealthCareProfessionalRepository healthCareProfessionalRepository, IClientLeadRepository clientLeadRepository)
    {
      this.viewName = viewName;
      this.healthCareProfessionalRepository = healthCareProfessionalRepository;
      this.clientLeadRepository = clientLeadRepository;
      this.healthCareProfessionalId = healthProfessionalId;
      this.clientLeadId = clientLeadId;
      SetModel();
      this.viewHtml = HtmlHelperExtensions.RenderPartialToString("partials/_" + viewName + "Partial", this, controllerContext);
    }
    public AdminModel(string viewName, int healthProfessionalId, ClientLead clientLead, ControllerContext controllerContext, IHealthCareProfessionalRepository healthCareProfessionalRepository, IClientLeadRepository clientLeadRepository)
    {
      this.viewName = viewName;
      this.healthCareProfessionalRepository = healthCareProfessionalRepository;
      this.clientLeadRepository = clientLeadRepository;
      this.healthCareProfessionalId = healthProfessionalId;
      this.clientLead = clientLead;
      this.controllerContext = controllerContext;
    }
    public AdminModel(string viewName, HealthCareProfessional healthCareProfessional, ControllerContext controllerContext, IHealthCareProfessionalRepository healthCareProfessionalRepository)
    {
      this.viewName = viewName;
      this.healthCareProfessional = healthCareProfessional;
      this.healthCareProfessionalRepository = healthCareProfessionalRepository;
      this.controllerContext = controllerContext;
    }
    public AdminModel(string viewName, HealthCareProfessionalTitle healthCareProfessionalTitle, ControllerContext controllerContext, IHealthCareProfessionalRepository healthCareProfessionalRepository)
    {
      this.viewName = viewName;
      this.healthCareProfessionalTitle = healthCareProfessionalTitle;
      this.healthCareProfessionalRepository = healthCareProfessionalRepository;
      this.controllerContext = controllerContext;
    }

    private void SetModel()
    {
      switch (viewName)
      {
        case "healthCareProfessionals":
          GetHealthCareProfessionalsModel();
          break;
        case "healthCareProfessional":
          GetHealthCareProfessionalModel();
          break;
        case "clientLeads":
          GetClientLeads();
          break;
        case "clientLeadsList":
          GetClientLeads();
          break;
        case "clientLead":
          GetClientLead();
          break;
        case "clientLeadModal":
          GetClientLead();
          break;
        default:
          GetHealthCareProfessionalsModel();
          break;
      }
    }

    private void GetClientLeads()
    {
      healthCareProfessional = healthCareProfessionalRepository.GetHealthCareProfessionalById(healthCareProfessionalId);
      clientLeads = clientLeadRepository.GetClientLeadsByHealthCareProfessional(healthCareProfessionalId);
    }

    private void GetClientLead()
    {
      modelIsValid = true;
      clientLead = clientLeadRepository.GetClientLeadById(healthCareProfessionalId, clientLeadId);
    }
    private void GetHealthCareProfessionalsModel()
    {
      healthCareProfessionals = healthCareProfessionalRepository.GetHealthCareProfessionals();
    }

    private void GetHealthCareProfessionalModel()
    {
      if (!string.IsNullOrEmpty(healthCareProfessionalUrl))
        healthCareProfessional =
          healthCareProfessionalRepository.GetHealthCareProfessionalByUrl(healthCareProfessionalUrl);
      else
        healthCareProfessional = healthCareProfessionalRepository.GetHealthCareProfessionalById(healthCareProfessionalId);

      healthCareProfessional.ProfessionalTitles = new List<HealthCareProfessionalTitle>();
      healthCareProfessional.ProfessionalTitles = healthCareProfessionalRepository.GetHealthCareProfessionalTitles(healthCareProfessional.HealthCareProfessionalId);
    }

    public void SaveHealthCareProfessional()
    {
      healthCareProfessional.ModifiedByUserId = controllerContext.HttpContext.User.Identity.GetUserId();
      var hp = healthCareProfessionalRepository.SaveHealthCareProfessional(healthCareProfessional);
      validationResults = healthCareProfessionalRepository.ValidationResults;
      //if (validationResults.Any())
      //this.healthCareProfessional = hp;
      this.healthCareProfessional.HealthCareProfessionalId = hp.HealthCareProfessionalId;
      this.healthCareProfessional.DateUpdated = hp.DateUpdated;
      this.viewHtml = HtmlHelperExtensions.RenderPartialToString("partials/_" + viewName + "Partial", this, controllerContext);
    }
    public void SaveHealthCareProfessionalTitle()
    {
      healthCareProfessionalTitle.ModifiedByUserId = controllerContext.HttpContext.User.Identity.GetUserId();
      var hp = healthCareProfessionalRepository.SaveHealthCareProfessionalTitle(healthCareProfessionalTitle);
      healthCareProfessional =
        healthCareProfessionalRepository.GetHealthCareProfessionalById(
          healthCareProfessionalTitle.HealthCareProfessionalId);
      healthCareProfessional.ProfessionalTitles = hp;
      validationResults = healthCareProfessionalRepository.ValidationResults;
      this.viewHtml = HtmlHelperExtensions.RenderPartialToString("partials/_" + viewName + "Partial", this, controllerContext);
    }

    public void SaveClientLead(string clientLeadTypeDescription)
    {
      if (controllerContext != null)
        clientLead.ModifiedByUserId = controllerContext.HttpContext.User.Identity.GetUserId();

      var clientLeadType = clientLeadRepository.GetClientLeadTypeByDescription(clientLeadTypeDescription);
      if (clientLeadType == null)
      {
        var clt = new ClientLeadType
        {
          ClientLeadTypeDescription =  clientLeadTypeDescription,
          ModifiedByUserId = controllerContext.HttpContext.User.Identity.GetUserId()
      };
        clientLeadType = clientLeadRepository.SaveClientLeadType(clt);
      }
      if (clientLeadType != null)
      {
        clientLead.ClientLeadTypeId = clientLeadType.ClientLeadTypeId;
        modelIsValid = true;
        clientLeadRepository.SaveClientLead(clientLead);
      }
      validationResults = clientLeadRepository.ValidationResults;
      modelIsValid = validationResults.Count == 0;

      this.viewHtml = HtmlHelperExtensions.RenderPartialToString("partials/_" + viewName + "Partial", this, controllerContext);
    }
    private string viewName;

    public string ViewName
    {
      get { return viewName; }
    }
    private int healthCareProfessionalId;
    private string healthCareProfessionalUrl;
    private int clientLeadId;
    private IHealthCareProfessionalRepository healthCareProfessionalRepository;
    private IClientLeadRepository clientLeadRepository;
    private string viewHtml;
    public string ViewHtml
    {
      get { return viewHtml; }
      set { viewHtml = value; }
    }
    private IList<HealthCareProfessional> healthCareProfessionals;

    public IList<HealthCareProfessional> HealthCareProfessionals
    {
      get { return healthCareProfessionals; }
      set { healthCareProfessionals = value; }
    }
    private HealthCareProfessional healthCareProfessional;

    public HealthCareProfessional HealthCareProfessional
    {
      get { return healthCareProfessional; }
      set { healthCareProfessional = value; }
    }
    private HealthCareProfessionalTitle healthCareProfessionalTitle;

    public HealthCareProfessionalTitle HealthCareProfessionalTitle
    {
      get { return healthCareProfessionalTitle; }
      set { healthCareProfessionalTitle = value; }
    }

    private IList<ValidationResult> validationResults;

    public IList<ValidationResult> ValidationResults
    {
      get { return validationResults; }
      set { validationResults = value; }
    }
    private ControllerContext controllerContext;
    private IList<ClientLead> clientLeads;

    public IList<ClientLead> ClientLeads
    {
      get { return clientLeads; }
      set { clientLeads = value; }
    }
    private ClientLead clientLead;

    public ClientLead ClientLead
    {
      get { return clientLead; }
      set { clientLead = value; }
    }
    private bool modelIsValid;

    public bool ModelIsValid
    {
      get { return modelIsValid; }
      set { modelIsValid = value; }
    }

  }
}