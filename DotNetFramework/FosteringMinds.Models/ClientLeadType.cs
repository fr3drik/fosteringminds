﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FosteringMinds.Models
{
  public class ClientLeadType : BaseEntity
  {
    public int ClientLeadTypeId { get; set; }
    [Required(ErrorMessage = "You must supply a lead type")]
    [MinLength(2)]
    public string ClientLeadTypeDescription { get; set; }
  }
}
