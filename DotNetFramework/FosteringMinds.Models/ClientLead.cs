﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FosteringMinds.Models
{
  public class ClientLead : BaseEntity
  {
    public int HealthCareProfessionalId { get; set; }
    public int ClientLeadTypeId { get; set; }
    public int ClientLeadId { get; set; }
    [Required]
    [MinLength(2)]
    public string ClientFirstName { get; set; }
    [Required]
    [MinLength(2)]
    public string ClientSurname { get; set; }
    [Required]
    public string ContactNumber { get; set; }
    public string EmailAddress { get; set; }
    public string Message { get; set; }
    public string GeoLocation { get; set; }
    public string Referrer { get; set; }
    [Required]
    public string ClientLeadTypeDescription { get; set; }
    public LeadStatus LeadStatus { get; set; }
  }

  public enum LeadStatus
  {
    Pending = 0,
    FollowedUp = 1
  }
}
