﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FosteringMinds.Models
{
  public class HealthCareProfessional : BaseEntity
  {
    public int HealthCareProfessionalId { get; set; }
    [Required(ErrorMessage = "First name is required")]
    [MinLength(2)]
    public string HealthCareProfessionalFirstName { get; set; }
    [Required(ErrorMessage = "Last name is required")]
    [MinLength(2)]
    public string HealthCareProfessionalLastName { get; set; }
    [MinLength(2)]
    public string HealthCareProfessionalTitle { get; set; }
    public string HealthCareProfessionalBio { get; set; }
    public string HealthCareProfessionalUrl { get; set; }
    public IList<HealthCareProfessionalTitle> ProfessionalTitles { get; set; }
  }
}
