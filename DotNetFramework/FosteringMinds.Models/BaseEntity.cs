﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FosteringMinds.Models
{
  public class BaseEntity
  {
    public DateTime DateCreated { get; set; }
    public DateTime DateUpdated { get; set; }
    public string ModifiedByUserId { get; set; }
  }
}
