﻿using Corsair;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using Dapper;

namespace FosteringMinds.Identity.Repositories
{
  public class UserLoginsRepository
  {
    private IDbManager _dbManager;

    public UserLoginsRepository()
    {

    }
    public UserLoginsRepository(IDbManager dbManager)
    {
      _dbManager = dbManager;
    }

    public int Delete(IdentityUser user, UserLoginInfo login)
    {
      string commandText = "DELETE FROM user_login WHERE user_id = @userId AND login_provider = @loginProvider AND ProviderKey = @providerKey;";

      return _dbManager.Connection.ExecuteScalar<int>(commandText, new { userId = user.Id, loginProvider = login.LoginProvider, providerKey = login.ProviderKey });
    }

    public int Delete(string userId)
    {
      string commandText = "DELETE FROM user_login WHERE user_id = @userId;";

      return _dbManager.Connection.ExecuteScalar<int>(commandText, new { userId = userId });
    }

    public int Insert(IdentityUser user, UserLoginInfo login)
    {
      string commandText = "INSERT INTO user_logins (login_provider, provider_key, user_id) VALUES (@loginProvider, @providerKey, @userId);";

      return _dbManager.Connection.ExecuteScalar<int>(commandText, new
      {
        loginProvider = login.LoginProvider,
        providerKey = login.ProviderKey,
        userId = user.Id
      });
    }

    public string FindUserIdByLogin(UserLoginInfo userLogin)
    {
      string commandText = "SELECT user_id FROM user_login WHERE login_provider = @loginProvider AND provider_key = @providerKey;";

      return _dbManager.Connection.Query<string>(commandText, new
      {
        loginProvider = userLogin.LoginProvider,
        providerKey = userLogin.ProviderKey
      }).First();
    }

    public List<UserLoginInfo> FindByUserId(string userId)
    {
      List<UserLoginInfo> logins = new List<UserLoginInfo>();
      string commandText = "SELECT login_provider as LoginProvider, provider_key as ProviderKey FROM user_login WHERE user_id = @userId;";

      return _dbManager.Connection.Query<UserLoginInfo>(commandText, new { userId = userId }).ToList();
    }
  }
}
