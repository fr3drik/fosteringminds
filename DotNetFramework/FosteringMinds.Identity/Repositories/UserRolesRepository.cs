﻿using Corsair;
using System.Collections.Generic;
using System.Linq;
using Dapper;

namespace FosteringMinds.Identity.Repositories
{
  public class UserRolesRepository
  {
    private IDbManager _dbManager;

    public UserRolesRepository()
    {

    }
    public UserRolesRepository(IDbManager dbManager)
    {
      _dbManager = dbManager;
    }

    public List<string> FindByUserId(string userId)
    {
      List<string> roles = new List<string>();
      //TODO: This probably does not work, and may need testing.
      string commandText = @"SELECT r.role_name FROM user u JOIN user_role AR ON u.user_id=AR.user_id 
                            JOIN role r ON AR.role_id = R.role_id
                            WHERE u.user_id = @userId;";

      return _dbManager.Connection.Query<string>(commandText, new { userId = userId }).ToList();
    }

    public int Delete(string userId)
    {
      string commandText = "DELETE FROM user_role WHERE user_id = @userId;";

      return _dbManager.Connection.ExecuteScalar<int>(commandText, new { userId = userId });
    }

    public int Insert(IdentityUser user, string roleId)
    {
      string commandText = "INSERT INTO user_role (user_id, role_id) VALUES (@userId, @roleId);";

      return _dbManager.Connection.ExecuteScalar<int>(commandText, new { userId = user.Id, roleId = roleId });
    }
  }
}
