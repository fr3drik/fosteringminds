﻿using Corsair;
using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;

namespace FosteringMinds.Identity.Repositories
{
  public class RoleRepository<TRole>
      where TRole : IdentityRole
  {
    private IDbManager _dbManager;

    public RoleRepository()
    {

    }
    public RoleRepository(IDbManager dbManager)
    {
      _dbManager = dbManager;
    }

    public int Delete(string roleId)
    {
      string commandText = "DELETE FROM role WHERE role_id = @id;";

      return _dbManager.Connection.Execute(commandText, new { id = roleId });
    }

    public int Insert(IdentityRole role)
    {
      string commandText = "INSERT INTO role (role_id, role_name) VALUES (@id, @name);";

      return _dbManager.Connection.Execute(commandText, new { name = role.Name, id = role.Id });
    }

    public string GetRoleName(string roleId)
    {
      string commandText = "SELECT role_name FROM role WHERE role_id = @id;";

      return _dbManager.Connection.Query<string>(commandText, new { id = roleId }).First();
    }

    public string GetRoleId(string roleName)
    {
      string roleId = null;
      string commandText = "SELECT role_id FROM roles WHERE role_name = @name;";

      var result = _dbManager.Connection.Query<string>(commandText, new { name = roleName }).First();
      if (result != null)
      {
        return Convert.ToString(result);
      }

      return roleId;
    }

    public IdentityRole GetRoleById(string roleId)
    {
      var roleName = GetRoleName(roleId);
      IdentityRole role = null;

      if (roleName != null)
      {
        role = new IdentityRole(roleName, roleId);
      }

      return role;

    }

    public IdentityRole GetRoleByName(string roleName)
    {
      var roleId = GetRoleId(roleName);
      IdentityRole role = null;

      if (roleId != null)
      {
        role = new IdentityRole(roleName, roleId);
      }

      return role;
    }

    public int Update(IdentityRole role)
    {
      string commandText = "UPDATE role SET role_name = @name WHERE role_id = @id;";

      return _dbManager.Connection.ExecuteScalar<int>(commandText, new { id = role.Id });
    }
    public IQueryable<TRole> GetAll()
    {
      List<TRole> roles = new List<TRole>();
      string commandText = "SELECT role_id, role_name FROM role;";

      var rows = _dbManager.Connection.Query<TRole>(commandText, null);

      return roles.AsQueryable<TRole>();
    }
  }
}
