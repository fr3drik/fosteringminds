﻿using Corsair;
using System.Linq;
using System.Security.Claims;
using Dapper;

namespace FosteringMinds.Identity.Repositories
{
  public class UserClaimsRepository
  {
    private IDbManager _dbManager;

    public UserClaimsRepository()
    {

    }
    public UserClaimsRepository(IDbManager dbManager)
    {
      _dbManager = dbManager;
    }

    public ClaimsIdentity FindByUserId(string userId)
    {
      ClaimsIdentity claims = new ClaimsIdentity();
      string commandText = "SELECT * FROM user_claim WHERE user_id = @userId;";
      claims = _dbManager.Connection.Query<ClaimsIdentity>(commandText, new { UserId = userId }).FirstOrDefault();
      return claims;
    }

    public int Delete(string userId)
    {
      string commandText = "DELETE FROM user_claim WHERE user_id = @userId;";

      return _dbManager.Connection.ExecuteScalar<int>(commandText, new { userId = userId });
    }

    public int Insert(Claim userClaim, string userId)
    {
      string commandText = "INSERT INTO user_claim (claim_value, claim_type, UserId) VALUES (@value, @type, @userId);";

      return _dbManager.Connection.ExecuteScalar<int>(commandText, new { value = userClaim.Value, type = userClaim.Type, userId = userId });
    }

    public int Delete(IdentityUser user, Claim claim)
    {
      string commandText = "DELETE FROM user_claim WHERE user_id = @userId AND @ClaimValue = @value AND claim_type = @type;";

      return _dbManager.Connection.ExecuteScalar<int>(commandText, new { userId = user.Id, value = claim.Value, type = claim.Type });
    }
  }
}
