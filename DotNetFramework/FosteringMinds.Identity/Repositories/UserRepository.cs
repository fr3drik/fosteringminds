﻿using Corsair;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Dapper;

namespace FosteringMinds.Identity.Repositories
{
  public class UserRepository<TUser>
           where TUser : IdentityUser
  {
    private IDbManager _dbManager;
    public UserRepository(IDbManager dbManager)
    {
      _dbManager = dbManager;
    }
    public string GetUserName(string userId)
    {
      string commandText = "SELECT user_name As UserName FROM user WHERE user_id = @id;";
      return _dbManager.Connection.Query<string>(commandText, new { id = userId }).First();
    }
    public string GetUserId(string userName)
    {
      if (userName != null)
        userName = userName.ToLower();

      string commandText = "SELECT user_id As Id FROM user WHERE user_name = @name;";
      File.WriteAllText(@"D:\DataDump\userName.txt", userName);
      return _dbManager.Connection.Query<string>(commandText, new { name = userName }).First();
    }
    public TUser GetUserById(string userId)
    {
      string commandText = "SELECT user_id As Id, user_name As UserName, password_hash As PasswordHash, security_stamp as SecurityStamp, email as Email, phone_number as PhoneNumber, email_confirmed as EmailConfirmed, phone_number_confirmed as PhoneNumberConfirmed, access_failed_count as AccessFailedCount, lockout_enabled as LockoutEnabled, lockout_end_date_utc as LockoutEndDate, two_factor_enabled as TwoFactorAuthEnabled FROM user WHERE user_id = @id;";
      return _dbManager.Connection.Query<TUser>(commandText, new { id = userId }).First();
    }
    public List<TUser> GetUserByName(string userName)
    {
      if (userName != null)
        userName = userName.ToLower();

      List<TUser> users = new List<TUser>();
      string commandText = "SELECT user_id As Id, user_name As UserName, password_hash As PasswordHash, security_stamp as SecurityStamp, email as Email, phone_number as PhoneNumber, email_confirmed as EmailConfirmed, phone_number_confirmed as PhoneNumberConfirmed, access_failed_count as AccessFailedCount, lockout_enabled as LockoutEnabled, lockout_end_date_utc as LockoutEndDate, two_factor_enabled as TwoFactorAuthEnabled FROM user WHERE user_name = @name;";
      return _dbManager.Connection.Query<TUser>(commandText, new { name = userName }).ToList();
    }
    public List<TUser> GetUserByEmail(string email)
    {
      if (email != null)
        email = email.ToLower();

      List<TUser> users = new List<TUser>();
      string commandText = "SELECT user_id As Id, user_name As UserName, password_hash As PasswordHash, security_stamp as SecurityStamp, email as Email, phone_number as PhoneNumber, email_confirmed as EmailConfirmed, phone_number_confirmed as PhoneNumberConfirmed, access_failed_count as AccessFailedCount, lockout_enabled as LockoutEnabled, lockout_end_date_utc as LockoutEndDate, two_factor_enabled as TwoFactorAuthEnabled FROM user WHERE email = @email;";
      return _dbManager.Connection.Query<TUser>(commandText, new { email = email }).ToList();
    }
    public IQueryable<TUser> GetAll()
    {
      List<TUser> users = new List<TUser>();
      string commandText = "SELECT user_id As Id, user_name As UserName, password_hash As PasswordHash, security_stamp as SecurityStamp, email as Email, phone_number as PhoneNumber, email_confirmed as EmailConfirmed, phone_number_confirmed as PhoneNumberConfirmed, access_failed_count as AccessFailedCount, lockout_enabled as LockoutEnabled, lockout_end_date_utc as LockoutEndDate, two_factor_enabled as TwoFactorAuthEnabled FROM user;";
      return _dbManager.Connection.Query<TUser>(commandText, null).AsQueryable();
    }
    public string GetPasswordHash(string userId)
    {
      string commandText = "SELECT password_hash as PasswordHash FROM user WHERE user_id = @id;";
      return _dbManager.Connection.Query<string>(commandText, new { id = userId }).First();
    }
    public int SetPasswordHash(string userId, string passwordHash)
    {
      string commandText = "UPDATE user SET password_hash = @pwdHash WHERE user_id = @id;";
      return _dbManager.Connection.ExecuteScalar<int>(commandText, new { id = userId, pwdHash = passwordHash });
    }
    public string GetSecurityStamp(string userId)
    {
      string commandText = "SELECT security_stamp FROM user WHERE user_id = @id;";
      return _dbManager.Connection.Query<string>(commandText, new { id = userId }).First();
    }
    public int Insert(TUser user)
    {
      var lowerCaseEmail = user.Email == null ? null : user.Email.ToLower();

      string commandText = @"
            INSERT INTO user(user_id, user_name, password_hash, security_stamp, email, 
                                        email_confirmed,phone_number_confirmed,two_factor_enabled,lockout_enabled,access_failed_count)
            VALUES (@id, @name, @pwdHash, @SecStamp, @email, @emailconfirmed,@phoneNumberConfirmed,@twoFactorEnabled,@lockoutEnabled,@accessFailedCount);";


      return _dbManager.Connection.ExecuteScalar<int>(commandText,
          new
          {
            name = user.UserName,
            id = user.Id,
            pwdHash = user.PasswordHash,
            SecStamp = user.SecurityStamp,
            email = user.Email,
            emailConfirmed = user.EmailConfirmed,
            phoneNumberConfirmed = user.PhoneNumberConfirmed,
            twoFactorEnabled = user.TwoFactorAuthEnabled,
            lockoutEnabled = user.LockoutEnabled,
            accessFailedCount = user.AccessFailedCount
          });
    }
    private int Delete(string userId)
    {
      string commandText = "DELETE FROM user WHERE user_id = @userId;";
      return _dbManager.Connection.ExecuteScalar<int>(commandText, new { id = userId });
    }
    public int Delete(TUser user)
    {
      return Delete(user.Id);
    }
    public int Update(TUser user)
    {
      var lowerCaseEmail = user.Email == null ? null : user.Email.ToLower();

      string commandText = @"
                UPDATE user
                   SET user_name = @userName, password_hash = @pswHash, 
                        security_stamp = @secStamp, 
                        email = @email, 
                        email_confirmed = @emailconfirmed, 
                        phone_number_confirmed = @phoneNumberConfirmed, 
                        two_factor_enabled = @twoFactorEnabled,
                        lockout_enabled = @lockoutEnabled,
                        access_failed_count = @accessFailedCount
                 WHERE user_id = @userId;";

      return _dbManager.Connection.ExecuteScalar<int>(commandText, new
      {
        userName = user.UserName,
        pswHash = user.PasswordHash,
        secStamp = user.SecurityStamp,
        userId = user.Id,
        email = user.Email,
        emailConfirmed = user.EmailConfirmed,
        phoneNumberConfirmed = user.PhoneNumberConfirmed,
        twoFactorEnabled = user.TwoFactorAuthEnabled,
        lockoutEnabled = user.LockoutEnabled,
        accessFailedCount = user.AccessFailedCount
      });
    }
  }
}
