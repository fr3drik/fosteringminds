﻿using Corsair;
using FosteringMinds.Identity.Repositories;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FosteringMinds.Identity.Stores
{
  public class UserStore<TUser> : IUserLoginStore<TUser>,
        IUserClaimStore<TUser>,
        IUserRoleStore<TUser>,
        IUserPasswordStore<TUser>,
        IUserSecurityStampStore<TUser>,
        IQueryableUserStore<TUser>,
        IUserEmailStore<TUser>,
        IUserStore<TUser>,
        IUserLockoutStore<TUser, string>,
        IUserTwoFactorStore<TUser, string>
        where TUser : IdentityUser
  {
    private UserRepository<TUser> userRepository;
    private RoleRepository<IdentityRole> roleRepository;
    private UserRolesRepository userRolesRepository;
    private UserClaimsRepository userClaimsRepository;
    private UserLoginsRepository userLoginsRepository;
    public IDbManager DbManager { get; private set; }

    public IQueryable<TUser> Users
    {
      get
      {
        return userRepository.GetAll();
      }
    }
    public UserStore()
    {
    }
    public UserStore(IDbManager dbManager)
    {
      DbManager = dbManager;
      userRepository = new UserRepository<TUser>(dbManager);
      roleRepository = new RoleRepository<IdentityRole>(dbManager);
      userRolesRepository = new UserRolesRepository(dbManager);
      userClaimsRepository = new UserClaimsRepository(dbManager);
      userLoginsRepository = new UserLoginsRepository(dbManager);
    }
    public Task CreateAsync(TUser user)
    {
      if (user == null)
      {
        throw new ArgumentNullException("user");
      }

      userRepository.Insert(user);

      return Task.FromResult<object>(null);
    }
    public Task<TUser> FindByIdAsync(string userId)
    {
      if (string.IsNullOrEmpty(userId))
      {
        throw new ArgumentException("Null or empty argument: userId");
      }
      TUser result = userRepository.GetUserById(userId) as TUser;
      if (result != null)
      {
        return Task.FromResult<TUser>(result);
      }

      return Task.FromResult<TUser>(null);
    }
    public Task<TUser> FindByNameAsync(string userName)
    {
      if (string.IsNullOrEmpty(userName))
      {
        throw new ArgumentException("Null or empty argument: userName");
      }

      List<TUser> result = userRepository.GetUserByName(userName) as List<TUser>;


      if (result != null)
      {
        if (result.Count == 1)
        {
          return Task.FromResult<TUser>(result[0]);
        }
        else if (result.Count > 1)
        {
          //todo: exception for release mode?
#if DEBUG
          throw new ArgumentException("More than one user record returned.");
#endif
        }
      }

      return Task.FromResult<TUser>(null);
    }
    public Task UpdateAsync(TUser user)
    {
      if (user == null)
      {
        throw new ArgumentNullException("user");
      }

      userRepository.Update(user);

      return Task.FromResult<object>(null);
    }

    public void Dispose()
    {
      if (DbManager != null)
      {
        DbManager.Connection.Dispose();
        DbManager = null;
      }
    }
    public Task AddClaimAsync(TUser user, Claim claim)
    {
      if (user == null)
      {
        throw new ArgumentNullException("user");
      }

      if (claim == null)
      {
        throw new ArgumentNullException("user");
      }

      userClaimsRepository.Insert(claim, user.Id);

      return Task.FromResult<object>(null);
    }
    public Task<IList<Claim>> GetClaimsAsync(TUser user)
    {
      ClaimsIdentity identity = userClaimsRepository.FindByUserId(user.Id);
      return Task.FromResult<IList<Claim>>(identity != null ? identity.Claims.ToList() : new List<Claim>());
    }
    public Task RemoveClaimAsync(TUser user, Claim claim)
    {
      if (user == null)
      {
        throw new ArgumentNullException("user");
      }

      if (claim == null)
      {
        throw new ArgumentNullException("claim");
      }

      userClaimsRepository.Delete(user, claim);

      return Task.FromResult<object>(null);
    }
    public Task AddLoginAsync(TUser user, UserLoginInfo login)
    {
      if (user == null)
      {
        throw new ArgumentNullException("user");
      }

      if (login == null)
      {
        throw new ArgumentNullException("login");
      }

      userLoginsRepository.Insert(user, login);

      return Task.FromResult<object>(null);
    }
    public Task<TUser> FindAsync(UserLoginInfo login)
    {
      if (login == null)
      {
        throw new ArgumentNullException("login");
      }

      var userId = userLoginsRepository.FindUserIdByLogin(login);
      if (userId != null)
      {
        TUser user = userRepository.GetUserById(userId) as TUser;
        if (user != null)
        {
          return Task.FromResult<TUser>(user);
        }
      }

      return Task.FromResult<TUser>(null);
    }
    public Task<IList<UserLoginInfo>> GetLoginsAsync(TUser user)
    {
      List<UserLoginInfo> userLogins = new List<UserLoginInfo>();
      if (user == null)
      {
        throw new ArgumentNullException("user");
      }

      List<UserLoginInfo> logins = userLoginsRepository.FindByUserId(user.Id);
      if (logins != null)
      {
        return Task.FromResult<IList<UserLoginInfo>>(logins);
      }

      return Task.FromResult<IList<UserLoginInfo>>(null);
    }
    public Task RemoveLoginAsync(TUser user, UserLoginInfo login)
    {
      if (user == null)
      {
        throw new ArgumentNullException("user");
      }

      if (login == null)
      {
        throw new ArgumentNullException("login");
      }

      userLoginsRepository.Delete(user, login);

      return Task.FromResult<Object>(null);
    }
    public Task AddToRoleAsync(TUser user, string roleName)
    {
      if (user == null)
      {
        throw new ArgumentNullException("user");
      }

      if (string.IsNullOrEmpty(roleName))
      {
        throw new ArgumentException("Argument cannot be null or empty: roleName.");
      }

      string roleId = roleRepository.GetRoleId(roleName);
      if (!string.IsNullOrEmpty(roleId))
      {
        userRolesRepository.Insert(user, roleId);
      }

      return Task.FromResult<object>(null);
    }
    public Task<IList<string>> GetRolesAsync(TUser user)
    {
      if (user == null)
      {
        throw new ArgumentNullException("user");
      }

      List<string> roles = userRolesRepository.FindByUserId(user.Id);
      {
        if (roles != null)
        {
          return Task.FromResult<IList<string>>(roles);
        }
      }

      return Task.FromResult<IList<string>>(null);
    }
    public Task<bool> IsInRoleAsync(TUser user, string role)
    {
      if (user == null)
      {
        throw new ArgumentNullException("user");
      }

      if (string.IsNullOrEmpty(role))
      {
        throw new ArgumentNullException("role");
      }

      List<string> roles = userRolesRepository.FindByUserId(user.Id);
      {
        if (roles != null && roles.Contains(role))
        {
          return Task.FromResult<bool>(true);
        }
      }

      return Task.FromResult<bool>(false);
    }
    public Task RemoveFromRoleAsync(TUser user, string role)
    {
      throw new NotImplementedException();
    }
    public Task DeleteAsync(TUser user)
    {
      if (user != null)
      {
        userRepository.Delete(user);
      }

      return Task.FromResult<Object>(null);
    }
    public Task<string> GetPasswordHashAsync(TUser user)
    {
      string passwordHash = userRepository.GetPasswordHash(user.Id);

      return Task.FromResult<string>(passwordHash);
    }
    public Task<bool> HasPasswordAsync(TUser user)
    {
      var hasPassword = !string.IsNullOrEmpty(userRepository.GetPasswordHash(user.Id));

      return Task.FromResult<bool>(Boolean.Parse(hasPassword.ToString()));
    }
    public Task SetPasswordHashAsync(TUser user, string passwordHash)
    {
      user.PasswordHash = passwordHash;

      return Task.FromResult<Object>(null);
    }
    public Task SetSecurityStampAsync(TUser user, string stamp)
    {
      user.SecurityStamp = stamp;

      return Task.FromResult(0);

    }
    public Task<string> GetSecurityStampAsync(TUser user)
    {
      return Task.FromResult(user.SecurityStamp);
    }
    public Task SetEmailAsync(TUser user, string email)
    {
      user.Email = email;
      userRepository.Update(user);

      return Task.FromResult(0);
    }
    public Task<string> GetEmailAsync(TUser user)
    {
      return Task.FromResult(user.Email);
    }
    public Task<bool> GetEmailConfirmedAsync(TUser user)
    {
      return Task.FromResult(user.EmailConfirmed);
    }
    public Task SetEmailConfirmedAsync(TUser user, bool confirmed)
    {
      user.EmailConfirmed = confirmed;
      userRepository.Update(user);

      return Task.FromResult(0);
    }
    public Task<TUser> FindByEmailAsync(string email)
    {
      if (String.IsNullOrEmpty(email))
      {
        throw new ArgumentNullException("email");
      }

      List<TUser> result = userRepository.GetUserByEmail(email) as List<TUser>;
      if (result != null && result.Count > 0)
      {
        return Task.FromResult<TUser>(result[0]);
      }

      return Task.FromResult<TUser>(null);
    }


    public Task<int> GetAccessFailedCountAsync(TUser user)
    {
      return Task.FromResult(user.AccessFailedCount);
    }

    public Task<bool> GetLockoutEnabledAsync(TUser user)
    {
      return Task.FromResult(user.LockoutEnabled);
    }

    public Task<DateTimeOffset> GetLockoutEndDateAsync(TUser user)
    {
      return Task.FromResult(user.LockoutEndDate ?? new DateTimeOffset());
    }

    public Task<int> IncrementAccessFailedCountAsync(TUser user)
    {
      user.AccessFailedCount++;
      return Task.FromResult(user.AccessFailedCount);
    }

    public Task ResetAccessFailedCountAsync(TUser user)
    {
      user.AccessFailedCount = 0;
      return Task.FromResult(0);
    }

    public Task SetLockoutEnabledAsync(TUser user, bool enabled)
    {
      user.LockoutEnabled = enabled;
      return Task.FromResult(0);
    }

    public Task SetLockoutEndDateAsync(TUser user, DateTimeOffset lockoutEnd)
    {
      user.LockoutEndDate = new DateTime(lockoutEnd.Ticks, DateTimeKind.Utc);
      return Task.FromResult(0);
    }
    public virtual Task SetTwoFactorEnabledAsync(TUser user, bool enabled)
    {
      user.TwoFactorAuthEnabled = enabled;
      return Task.FromResult(0);
    }

    public virtual Task<bool> GetTwoFactorEnabledAsync(TUser user)
    {
      return Task.FromResult(user.TwoFactorAuthEnabled);
    }
  }
}
