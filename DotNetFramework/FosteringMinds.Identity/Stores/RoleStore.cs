﻿using Corsair;
using FosteringMinds.Identity.Repositories;
using Microsoft.AspNet.Identity;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace FosteringMinds.Identity.Stores
{
  public class RoleStore<TRole> : IQueryableRoleStore<TRole>
             where TRole : IdentityRole
  {
    private RoleRepository<TRole> roleRepository;
    public IDbManager DbManager { get; set; }
    public IQueryable<TRole> Roles
    {
      get
      {
        return roleRepository.GetAll();
      }
    }
    public RoleStore()
    {
      
    }
    public RoleStore(IDbManager dbManager)
    {
      this.DbManager = dbManager;
      this.roleRepository = new RoleRepository<TRole>(dbManager);
    }
    public Task CreateAsync(TRole role)
    {
      if (role == null)
      {
        throw new ArgumentNullException("role");
      }

      roleRepository.Insert(role);

      return Task.FromResult<object>(null);
    }
    public Task DeleteAsync(TRole role)
    {
      if (role == null)
      {
        throw new ArgumentNullException("user");
      }

      roleRepository.Delete(role.Id);

      return Task.FromResult<Object>(null);
    }
    public Task<TRole> FindByIdAsync(string roleId)
    {
      TRole result = roleRepository.GetRoleById(roleId) as TRole;

      return Task.FromResult<TRole>(result);
    }
    public Task<TRole> FindByNameAsync(string roleName)
    {
      TRole result = roleRepository.GetRoleByName(roleName) as TRole;

      return Task.FromResult<TRole>(result);
    }
    public Task UpdateAsync(TRole role)
    {
      if (role == null)
      {
        throw new ArgumentNullException("user");
      }

      roleRepository.Update(role);

      return Task.FromResult<Object>(null);
    }
    public void Dispose()
    {
      if (DbManager.Connection != null)
      {
        DbManager.Connection.Dispose();
        DbManager = null;
      }
    }
  }
}
