﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Speech.Synthesis;
using System.Speech.Recognition;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace FosteringMinds.ConsoleUI
{
  class Program
  {
    static bool completed;
    static void Main(string[] args)
    {
      //var i = DeserializeXMLFileToObject<ICD10CMtabular>("icd10cm_tabular_2017.xml");
      //Console.WriteLine(i.version);
      //SpeechSynthesizer speaker = new SpeechSynthesizer();
      //speaker.Rate = 1;
      //speaker.Volume = 100;
      //var text = "Candice is my liefie";

      //speaker.Speak(text);
      //Console.WriteLine(text);

      //SpeechRecognitionEngine recognizer = new SpeechRecognitionEngine();
      //Grammar dictationGrammar = new DictationGrammar();
      ////dictationGrammar.Enabled = true;
      //recognizer.LoadGrammar(dictationGrammar);
      //recognizer.SetInputToWaveFile(@"D:\Projects\CorsairSolutions\VoiceRecordings\test01.wav");
      //RecognitionResult result = recognizer.Recognize();
      //Console.WriteLine(result.Text);

      using (SpeechRecognitionEngine recognizer =
        new SpeechRecognitionEngine())
      {

        // Create and load a grammar.
        Grammar dictation = new DictationGrammar();
        dictation.Name = "Dictation Grammar";

        recognizer.LoadGrammar(dictation);

        // Configure the input to the recognizer.
        recognizer.SetInputToWaveFile(@"D:\Projects\CorsairSolutions\VoiceRecordings\test01.wav");

        // Attach event handlers for the results of recognition.
        recognizer.SpeechRecognized +=
          new EventHandler<SpeechRecognizedEventArgs>(recognizer_SpeechRecognized);
        recognizer.RecognizeCompleted +=
          new EventHandler<RecognizeCompletedEventArgs>(recognizer_RecognizeCompleted);

        // Perform recognition on the entire file.
        Console.WriteLine("Starting asynchronous recognition...");
        completed = false;
        recognizer.RecognizeAsync();

        // Keep the console window open.
        while (!completed)
        {
          Console.ReadLine();
        }
        Console.WriteLine("Done.");
      }

      Console.WriteLine();
      Console.WriteLine("Press any key to exit...");
      Console.ReadKey();
    }
    static void recognizer_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
    {
      if (e.Result != null && e.Result.Text != null)
      {
        Console.WriteLine("  Recognized text =  {0}", e.Result.Text);
      }
      else
      {
        Console.WriteLine("  Recognized text not available.");
      }
    }

    // Handle the RecognizeCompleted event.
    static void recognizer_RecognizeCompleted(object sender, RecognizeCompletedEventArgs e)
    {
      if (e.Error != null)
      {
        Console.WriteLine("  Error encountered, {0}: {1}",
        e.Error.GetType().Name, e.Error.Message);
      }
      if (e.Cancelled)
      {
        Console.WriteLine("  Operation cancelled.");
      }
      if (e.InputStreamEnded)
      {
        Console.WriteLine("  End of stream encountered.");
      }
      completed = true;
    }
    public static T DeserializeXMLFileToObject<T>(string XmlFilename)
    {
      T returnObject = default(T);
      if (string.IsNullOrEmpty(XmlFilename)) return default(T);

      try
      {
        StreamReader xmlStream = new StreamReader(XmlFilename);
        XmlSerializer serializer = new XmlSerializer(typeof(T));
        returnObject = (T)serializer.Deserialize(xmlStream);
      }
      catch (Exception ex)
      {
        //ExceptionLogger.WriteExceptionToConsole(ex, DateTime.Now);
        Console.WriteLine(ex);
      }
      return returnObject;
    }
    static void GetElem(XmlNode node)
    {
      foreach (XmlNode e in node.ChildNodes)
      {
        Console.WriteLine(e.Name);
        GetElem(e);
      }
    }
  }
}
