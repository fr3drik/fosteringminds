﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Http;
using Autofac;
using Autofac.Integration.Mvc;
using Corsair;
using FosteringMinds.Repositories;
using FosteringMinds.WebUI;

namespace FosteringMinds.ClientWebUI
{
  public class MvcApplication : System.Web.HttpApplication
  {
    protected void Application_Start()
    {
      var builder = new ContainerBuilder();

      var config = GlobalConfiguration.Configuration;

      builder.RegisterModelBinders(Assembly.GetExecutingAssembly());
      builder.RegisterModelBinderProvider();
      builder.RegisterControllers(Assembly.GetExecutingAssembly());

      //builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
      builder.RegisterType<CounsellorRepository>().As<ICounsellorRepository>();
      builder.RegisterType<DbManager.MySql.DbManager>().As<IDbManager>()
          .WithParameter("connectionStringName", "fosteringminds");

      IContainer container = builder.Build();
      DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
      AreaRegistration.RegisterAllAreas();
      GlobalConfiguration.Configure(WebApiConfig.Register);
      RouteConfig.RegisterRoutes(RouteTable.Routes);
    }
  }
}
