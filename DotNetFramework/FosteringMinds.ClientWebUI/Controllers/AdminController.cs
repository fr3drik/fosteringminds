﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using FosteringMinds.ClientWebUI.Models;
using FosteringMinds.Models;
using FosteringMinds.Repositories;

namespace FosteringMinds.ClientWebUI.Controllers
{
  [Authorize]
  public class AdminController : Controller
  {
    // GET: Admin
    private AdminModel adminModel;
    private ICounsellorRepository counsellorRepository;

    public AdminController()
    {
      
    }

    public AdminController(ICounsellorRepository counsellorRepository)
    {
      this.counsellorRepository = counsellorRepository;
    }
    public ActionResult Index(string viewName)
    {
      adminModel = new AdminModel(counsellorRepository, viewName, ControllerContext);

      return View(adminModel);
    }

    [HttpPost]
    public JsonResult SaveCounsellor(HealthCareProfessional counsellor, string viewName)
    {
      adminModel = new AdminModel(counsellorRepository, viewName, ControllerContext);
      adminModel.SaveCounsellor(counsellor);
      return Json(data: adminModel);
    }
  }
}