﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FosteringMinds.ClientWebUI.Models;
using FosteringMinds.Repositories;

namespace FosteringMinds.ClientWebUI.Controllers
{
  [Authorize]
  public class HomeController : Controller
  {
    // GET: Home
    private DefaultModel defaultModel;
    private ICounsellorRepository counsellorRepository;
    public HomeController()
    {
      
    }

    public HomeController(ICounsellorRepository counsellorRepository)
    {
      this.counsellorRepository = counsellorRepository;
    }
    public ActionResult Index()
    {
      this.defaultModel = new DefaultModel(counsellorRepository, "index", ControllerContext);
      return View(defaultModel);
    }

    public ActionResult About()
    {
      this.defaultModel = new DefaultModel(counsellorRepository, "about", ControllerContext);
      return View(defaultModel);
    }

    public ActionResult Services()
    {
      this.defaultModel = new DefaultModel(counsellorRepository, "services", ControllerContext);
      return View(defaultModel);
    }

    public ActionResult Contact()
    {
      this.defaultModel = new DefaultModel(counsellorRepository, "contact", ControllerContext);
      return View(defaultModel);
    }
  }
}