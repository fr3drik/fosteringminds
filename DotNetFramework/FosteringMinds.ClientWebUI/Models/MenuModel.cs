﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FosteringMinds.ClientWebUI.Models.Dto;

namespace FosteringMinds.ClientWebUI.Models
{
  public class MenuModel
  {
    public MenuModel()
    {
      
    }

    public MenuModel(string viewName)
    {
      this.viewName = viewName;
      GetAdminMenus();
      SetActiveAdminMenu(this.viewName);
    }
    private string viewName;

    public string ViewName
    {
      get { return viewName; }
      set { viewName = value; }
    }
    private void GetAdminMenus()
    {
      menus = new List<MenuDTO>
      {
        new MenuDTO()
        {
          MenuDisplayText = "Counsellor Details",
          MenuViewName = "counsellorDetails",
          IsActive = false
        },
        new MenuDTO()
        {
          MenuDisplayText = "Counsellor Services",
          MenuViewName = "counsellorServices",
          IsActive = false
        },
        new MenuDTO()
        {
          MenuDisplayText = "Counsellor Contact Details",
          MenuViewName = "counsellorContactDetails",
          IsActive = false
        },
        new MenuDTO()
        {
          MenuDisplayText = "Web Responses",
          MenuViewName = "callsToAction",
          IsActive = false
        },
        new MenuDTO()
        {
          MenuDisplayText = "Practitioners",
          MenuViewName = "practitioners",
          IsActive = false
        },
        new MenuDTO()
        {
          MenuDisplayText = "Procedure Codes",
          MenuViewName = "procedureCodes",
          IsActive = false
        },
        new MenuDTO()
        {
          MenuDisplayText = "ICD10 Codes",
          MenuViewName = "icd10Codes",
          IsActive = false
        },
        new MenuDTO()
        {
          MenuDisplayText = "Clients",
          MenuViewName = "clients",
          IsActive = false
        },
        new MenuDTO()
        {
          MenuDisplayText = "Site Settings",
          MenuViewName = "settings",
          IsActive = false
        }
      };
    }

    private void SetActiveAdminMenu(string viewName)
    {
      if (menus.Any(a => a.MenuViewName == viewName))
      {
        menu = menus.First(a => a.MenuViewName == viewName);
      }
    }
    private IList<MenuDTO> menus;

    public IList<MenuDTO> Menus
    {
      get { return menus; }
      set { menus = value; }
    }
    private MenuDTO menu;

    public MenuDTO Menu
    {
      get { return menu; }
      set { menu = value; }
    }
  }
}