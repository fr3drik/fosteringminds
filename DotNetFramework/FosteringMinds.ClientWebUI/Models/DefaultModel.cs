﻿using FosteringMinds.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FosteringMinds.Repositories;

namespace FosteringMinds.ClientWebUI.Models
{
  public class DefaultModel
  {
    private ICounsellorRepository counsellorRepository;
    private ControllerContext controllerContext;
    public DefaultModel()
    {
      
    }

    public DefaultModel(ICounsellorRepository counsellorRepository, string viewName, ControllerContext controllerContext)
    {
      this.menuModel = new MenuModel(viewName);
      this.counsellorRepository = counsellorRepository;
      this.controllerContext = controllerContext;
      GetCounsellorByHost();
    }

    private void GetCounsellorByHost()
    {
      counsellor = counsellorRepository.GetCounsellorByDomain(controllerContext.HttpContext.Request.Url.Host);
    }
    private MenuModel menuModel;

    public MenuModel MenuModel
    {
      get { return menuModel; }
      set { menuModel = value; }
    }
    private HealthCareProfessional counsellor;

    public HealthCareProfessional Counsellor
    {
      get { return counsellor; }
    }
    private string host;

    public string Host
    {
      get { return host; }
      set { host = value; }
    }

    //public ICounsellorRepository CounsellorRepository
    //{
    //  get { return counsellorRepository; }
    //}
  }
}