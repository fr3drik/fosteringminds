﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FosteringMinds.ClientWebUI.Models
{
  public class HtmlHelperExtensions
  {
    public static string RenderPartialToString(string viewName, object model, ControllerContext ControllerContext)
    {
      if (string.IsNullOrEmpty(viewName))
        viewName = ControllerContext.RouteData.GetRequiredString("action");
      ViewDataDictionary ViewData = new ViewDataDictionary();
      TempDataDictionary TempData = new TempDataDictionary();
      ViewData.Model = model;

      using (StringWriter sw = new StringWriter())
      {
        ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
        if (viewResult.View != null)
        {
          ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
          viewResult.View.Render(viewContext, sw);

          return sw.GetStringBuilder().ToString();
        }
        else
        {
          return "View does not exist";
        }
      }

    }
  }
}