﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FosteringMinds.ClientWebUI.Models.Dto;
using FosteringMinds.Models;
using FosteringMinds.Repositories;

namespace FosteringMinds.ClientWebUI.Models
{
  public class AdminModel
  {
    public AdminModel()
    {
      
    }

    public AdminModel(ICounsellorRepository counsellorRepository, string viewName, ControllerContext context)
    {
      this.defaultModel = new DefaultModel(counsellorRepository, viewName, context);
      this.viewHtml = HtmlHelperExtensions.RenderPartialToString($"Partials/_{viewName}Partial", defaultModel, context);
    }

    public HealthCareProfessional SaveCounsellor(HealthCareProfessional counsellor)
    {
      var c = defaultModel.CounsellorRepository.SaveCounsellor(counsellor);
      if (c == null)
      {
        if (defaultModel.CounsellorRepository.ValidationResults.Any())
        {
          ModelState = new ModelStateDictionary();
          foreach (var validationResult in defaultModel.CounsellorRepository.ValidationResults)
          {
            ModelState.AddModelError(validationResult.MemberNames.First(), validationResult.ErrorMessage);            
          }
        }
      }
      return defaultModel.CounsellorRepository.SaveCounsellor(counsellor);
    }
    private DefaultModel defaultModel;

    public DefaultModel DefaultModel
    {
      get { return defaultModel; }
      set { defaultModel = value; }
    }

    private string viewHtml;
    public string ViewHtml { get { return viewHtml; } }

    private ModelStateDictionary modelState;
    public ModelStateDictionary ModelState
    {
      get { return modelState; }
      set { modelState = value; }
    }
  }
}