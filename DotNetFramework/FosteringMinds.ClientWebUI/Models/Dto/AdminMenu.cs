﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FosteringMinds.ClientWebUI.Models.Dto
{
  public class MenuDTO
  {
    public string MenuViewName { get; set; }
    public string MenuDisplayText { get; set; }
    public bool IsActive { get; set; }
  }
}