﻿using FosteringMinds.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FosteringMinds.Repositories
{
  public interface IHealthCareProfessionalRepository : IBaseRepository
  {
    IList<HealthCareProfessional> GetHealthCareProfessionals();
    HealthCareProfessional SaveHealthCareProfessional(HealthCareProfessional healthCareProfessional);
    HealthCareProfessional GetHealthCareProfessionalById(int healthCareProfessionalId);
    HealthCareProfessional GetHealthCareProfessionalByUrl(string url);
    IList<HealthCareProfessionalTitle> GetHealthCareProfessionalTitles(int healthCareProfessionalId);
    IList<HealthCareProfessionalTitle> SaveHealthCareProfessionalTitle(HealthCareProfessionalTitle healthCareProfessionalTitle);
    IList<HealthCareProfessionalTitle> DeleteHealthCareProfessionalTitle(HealthCareProfessionalTitle healthCareProfessionalTitle);
    IList<HealthCareProfessionalAccreditation> GetHealthCareProfessionalAccreditations(int healthCareProfessionalId);
    IList<HealthCareProfessionalAccreditation> SaveHealthCareProfessionalAccreditation(HealthCareProfessionalAccreditation healthCareProfessionalAccreditation);
    IList<HealthCareProfessionalAccreditation> DeleteHealthCareProfessionalAccreditation(HealthCareProfessionalAccreditation healthCareProfessionalAccreditation);
  }
}
