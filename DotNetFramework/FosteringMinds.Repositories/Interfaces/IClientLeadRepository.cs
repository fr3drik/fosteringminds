﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FosteringMinds.Models;

namespace FosteringMinds.Repositories.Interfaces
{
  public interface IClientLeadRepository : IBaseRepository
  {
    ClientLead SaveClientLead(ClientLead clientLead);
    ClientLead GetClientLeadById(int healthCareProfessionalId, int clientLeadId);
    IList<ClientLead> GetClientLeads();
    IList<ClientLead> GetClientLeadsByHealthCareProfessional(int healthCareProfessionalId);
    ClientLeadType SaveClientLeadType(ClientLeadType clientLeadType);
    ClientLeadType GetClientLeadTypeById(int clientLeadTypeId);
    ClientLeadType GetClientLeadTypeByDescription(string clientLeadTypeDescription);
    IList<ClientLeadType> GetClientLeadTypesByDescription(string clientLeadTypeDescription);
    IList<ClientLeadType> GetClientLeadTypes();
    IList<ClientLeadType> GetClientLeadTypesByHealthCareProfessional(int healthCareProfessionalId);
  }
}
