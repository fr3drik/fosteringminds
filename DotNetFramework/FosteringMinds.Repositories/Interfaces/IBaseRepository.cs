﻿using Corsair;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FosteringMinds.Repositories
{
  public interface IBaseRepository
  {
    IDbManager DbManager { get; }

    IList<ValidationResult> ValidationResults { get; set; }
  }
}
