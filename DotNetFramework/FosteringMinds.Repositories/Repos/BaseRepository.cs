﻿using Corsair;
using FosteringMinds.Repositories;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FosteringMinds.Repositories
{
  public class BaseRepository : IBaseRepository
  {
    private IDbManager dbManager;
    public IDbManager DbManager { get { return dbManager; } }
    public static readonly Logger Logger = LogManager.GetCurrentClassLogger();
    public BaseRepository()
    {

    }
    public BaseRepository(IDbManager dbManager)
    {
      this.dbManager = dbManager;
    }
    private IList<ValidationResult> validationResults;

    public IList<ValidationResult> ValidationResults
    {
      get { return validationResults; }
      set { validationResults = value; }
    }
  }
}
