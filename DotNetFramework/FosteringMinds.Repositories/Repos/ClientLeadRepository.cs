﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Corsair;
using Dapper;
using FosteringMinds.Models;
using FosteringMinds.Repositories.Interfaces;
using NLog;

namespace FosteringMinds.Repositories.Repos
{
  public class ClientLeadRepository : BaseRepository, IClientLeadRepository
  {
    public ClientLeadRepository()
    {
      
    }

    public ClientLeadRepository(IDbManager dbManager)
      :base(dbManager)
    {
      
    }

    public ClientLead SaveClientLead(ClientLead clientLead)
    {
      var validationContext = new ValidationContext(clientLead, null, null);
      ValidationResults = new List<ValidationResult>();
      Validator.TryValidateObject(clientLead, validationContext, ValidationResults);
      if (ValidationResults.Count == 0)
      {
        if (clientLead.ClientLeadId > 0)
        {
          try
          {
            return DbManager.Connection.Query<ClientLead>(ClientLeadConstants.CLIENTLEADUPDATE, new
            {
              healthcareprofessionalid = clientLead.HealthCareProfessionalId,
              clientleadtypeid = clientLead.ClientLeadTypeId,
              clientleadid = clientLead.ClientLeadId,
              clientfirstname = clientLead.ClientFirstName,
              clientsurname = clientLead.ClientSurname,
              contactnumber = clientLead.ContactNumber,
              emailaddress = clientLead.EmailAddress,
              message = clientLead.Message,
              geolocation = clientLead.GeoLocation,
              referrer = clientLead.Referrer,
              leadstatus = clientLead.LeadStatus,
              modifiedbyuserid = clientLead.ModifiedByUserId
            }).FirstOrDefault();
          }
          catch (Exception e)
          {
            Logger.Error(e);
            Logger.Error("Error occurred on update");
            ValidationResults.Add(new ValidationResult("There was an error in the system",
              new List<string> {"System Error"}));
          }
        }
        try
        {
          return DbManager.Connection.Query<ClientLead>(ClientLeadConstants.CLIENTLEADINSERT, new
          {
            healthcareprofessionalid = clientLead.HealthCareProfessionalId,
            clientleadtypeid = clientLead.ClientLeadTypeId,
            clientfirstname = clientLead.ClientFirstName,
            clientsurname = clientLead.ClientSurname,
            contactnumber = clientLead.ContactNumber,
            emailaddress = clientLead.EmailAddress,
            message = clientLead.Message,
            geolocation = clientLead.GeoLocation,
            referrer = clientLead.Referrer,
            leadstatus  = clientLead.LeadStatus,
            modifiedbyuserid = clientLead.ModifiedByUserId
          }).FirstOrDefault();
      }
        catch (Exception e)
      {
        Logger.Error(e);
        Logger.Error("Error occurred on insert");
        Logger.Error("Health Care Professional Id: " + clientLead.HealthCareProfessionalId);
        Logger.Error("Client Lead Type Id: " + clientLead.ClientLeadTypeId);
        Logger.Error("Client first name: " + clientLead.ClientFirstName);
        Logger.Error("Client surname: " + clientLead.ClientSurname);
        Logger.Error("Contact number: " + clientLead.ContactNumber);
        Logger.Error("Email address: " + clientLead.EmailAddress);
        Logger.Error("Message: " + clientLead.Message);
        Logger.Error("Geolocation: " + clientLead.GeoLocation);
        Logger.Error("Date created: " + clientLead.DateCreated);
        Logger.Error("Date updated: " + clientLead.DateUpdated);
        Logger.Error("Modified by user id: " + clientLead.ModifiedByUserId);
        Logger.Error(ClientLeadConstants.CLIENTLEADINSERT);
        ValidationResults.Add(new ValidationResult("There was an error in the system",
          new List<string> { "System Error" }));
      }
    }
      return null;
    }

    public ClientLead GetClientLeadById(int healthCareProfessionalId, int clientLeadId)
    {
      return DbManager.Connection.Query<ClientLead>(ClientLeadConstants.CLIENTLEADSELECTBYID, new { healthcareprofessionalid = healthCareProfessionalId, clientleadid = clientLeadId }).FirstOrDefault();
    }

    public IList<ClientLead> GetClientLeads()
    {
      throw new NotImplementedException();
    }

    public IList<ClientLead> GetClientLeadsByHealthCareProfessional(int healthCareProfessionalId)
    {
      return
        DbManager.Connection.Query<ClientLead>(ClientLeadConstants.CLIENTLEADSBYHEALTHCAREPROFESSIONAL,
          new {healthCareProfessionalId = healthCareProfessionalId}).ToList();
    }

    public ClientLeadType SaveClientLeadType(ClientLeadType clientLeadType)
    {
      var validationContext = new ValidationContext(clientLeadType, null, null);
      ValidationResults = new List<ValidationResult>();
      Validator.TryValidateObject(clientLeadType, validationContext, ValidationResults);
      if (ValidationResults.Count == 0)
      {
        if (clientLeadType.ClientLeadTypeId > 0)
        {
          return
            DbManager.Connection.Query<ClientLeadType>(ClientLeadConstants.CLIENTLEADTYPEUPDATE,
              new
              {
                clientleadtypeid = clientLeadType.ClientLeadTypeId,
                clientleadtypedescription = clientLeadType.ClientLeadTypeDescription,
                modifiedbyuserid = clientLeadType.ModifiedByUserId
              }).FirstOrDefault();
        }
        var clt = GetClientLeadTypeByDescription(clientLeadType.ClientLeadTypeDescription);
        if (clt != null)
        {
          return clt;
        }
        return DbManager.Connection.Query<ClientLeadType>(ClientLeadConstants.CLIENTLEADTYPEINSERT,
          new
          {
            clientleadtypedescription = clientLeadType.ClientLeadTypeDescription,
            modifiedbyuserid = clientLeadType.ModifiedByUserId
          }).FirstOrDefault();
      }
      return null;
    }

    public ClientLeadType GetClientLeadTypeById(int clientLeadTypeId)
    {
      throw new NotImplementedException();
    }
    public ClientLeadType GetClientLeadTypeByDescription(string clientLeadTypeDescription)
    {
      return DbManager.Connection.Query<ClientLeadType>(ClientLeadConstants.CLIENTLEADTYPESELECTBYDESCRIPTION,
        new
        {
          clientleadtypedescription = clientLeadTypeDescription
        }).FirstOrDefault();
    }

    public IList<ClientLeadType> GetClientLeadTypesByDescription(string clientLeadTypeDescription)
    {
      return DbManager.Connection.Query<ClientLeadType>(ClientLeadConstants.CLIENTLEADTYPESELECTBYDESCRIPTION,
        new
        {
          clientleadtypedescription = clientLeadTypeDescription
        }).ToList();
    }

    public IList<ClientLeadType> GetClientLeadTypes()
    {
      return DbManager.Connection.Query<ClientLeadType>(ClientLeadConstants.CLIENTLEADTYPESSELECT).ToList();
    }

    public IList<ClientLeadType> GetClientLeadTypesByHealthCareProfessional(int healthCareProfessionalId)
    {
      return
        DbManager.Connection.Query<ClientLeadType>(ClientLeadConstants.CLIENTLEADTYPESBYHEALTHCAREPROFESSIONALSELECT,
          new {healthCareProfessionalId = healthCareProfessionalId}).ToList();
    }
  }
}
