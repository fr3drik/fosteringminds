﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FosteringMinds.Models;
using Corsair;
using Dapper;

namespace FosteringMinds.Repositories
{
  public class HealthCareProfessionalRepository : BaseRepository, IHealthCareProfessionalRepository
  {
    public HealthCareProfessionalRepository()
    {

    }

    public HealthCareProfessionalRepository(IDbManager dbManager)
      : base(dbManager)
    {

    }

    public HealthCareProfessional GetHealthCareProfessionalById(int healthCareProfessionalId)
    {
      if(healthCareProfessionalId > 0)
      return
        DbManager.Connection.Query<HealthCareProfessional>(HealthCareProfessionalConstants.HEALTHCAREPROFESSIONALBYIDSELECT, new {healthCareProfessionalId = healthCareProfessionalId})
          .FirstOrDefault();
      return new HealthCareProfessional();
    }

    public HealthCareProfessional GetHealthCareProfessionalByUrl(string url)
    {
      return
        DbManager.Connection.Query<HealthCareProfessional>(HealthCareProfessionalConstants.HEALTHCAREPROFESSIONALBYURLSELECT, new {healthcareprofessionalurl = url})
          .FirstOrDefault();
    }

    public IList<HealthCareProfessional> GetHealthCareProfessionals()
    {
      return DbManager.Connection.Query<HealthCareProfessional>(HealthCareProfessionalConstants.HEALTHCAREPROFESSIONALSSELECT).ToList();
    }

    public HealthCareProfessional SaveHealthCareProfessional(HealthCareProfessional healthCareProfessional)
    {
      var validationContext = new ValidationContext(healthCareProfessional, null, null);
      ValidationResults = new List<ValidationResult>();
      Validator.TryValidateObject(healthCareProfessional, validationContext, ValidationResults);
      if (ValidationResults.Count == 0)
      {
        if (healthCareProfessional.HealthCareProfessionalId > 0)
        {
          //update
          return DbManager.Connection.Query<HealthCareProfessional>(HealthCareProfessionalConstants.HEALTHCAREPROFESSIONALUPDATE, new
          {
            healthcareprofessionalfirstname = healthCareProfessional.HealthCareProfessionalFirstName,
            healthcareprofessionallastname = healthCareProfessional.HealthCareProfessionalLastName,
            healthcareprofessionaltitle = healthCareProfessional.HealthCareProfessionalTitle,
            healthcareprofessionalbio = healthCareProfessional.HealthCareProfessionalBio,
            healthcareprofessionalurl = healthCareProfessional.HealthCareProfessionalUrl,
            healthcareprofessionalid = healthCareProfessional.HealthCareProfessionalId,
            modifiedbyuserid = healthCareProfessional.ModifiedByUserId
          }).FirstOrDefault();
        }
        //insert
        return DbManager.Connection.Query<HealthCareProfessional>(HealthCareProfessionalConstants.HEALTHCAREPROFESSIONALINSERT,
          new
          {
            healthcareprofessionalfirstname = healthCareProfessional.HealthCareProfessionalFirstName,
            healthcareprofessionallastname = healthCareProfessional.HealthCareProfessionalLastName,
            healthcareprofessionaltitle = healthCareProfessional.HealthCareProfessionalTitle,
            healthcareprofessionalbio = healthCareProfessional.HealthCareProfessionalBio,
            healthcareprofessionalurl = healthCareProfessional.HealthCareProfessionalUrl,
            modifiedbyuserid = healthCareProfessional.ModifiedByUserId
          },
          commandType: System.Data.CommandType.Text).FirstOrDefault();
      }
      return GetHealthCareProfessionalById(healthCareProfessional.HealthCareProfessionalId);
    }

    public IList<HealthCareProfessionalTitle> SaveHealthCareProfessionalTitle(HealthCareProfessionalTitle healthCareProfessionalTitle)
    {
      var healthCareProfessionalTitles =
        DbManager.Connection.Query<HealthCareProfessionalTitle>(
          HealthCareProfessionalTitleConstants.HEALTHCAREPROFESSIONALTITLESBYHEALTHCAREPROFESSIONAL,
          new {healthcareprofessionalid = healthCareProfessionalTitle.HealthCareProfessionalId}).ToList();
      if (
        healthCareProfessionalTitles.Any(h => h.HealthCareProfessionalTitleDescription.ToLower() == healthCareProfessionalTitle.HealthCareProfessionalTitleDescription))
      {
        return healthCareProfessionalTitles;
      }
      if(healthCareProfessionalTitle.HealthCareProfessionalTitleId == 0)
      {
        return
          DbManager.Connection.Query<HealthCareProfessionalTitle>(
            HealthCareProfessionalTitleConstants.HEALTHCAREPROFESSIONALTITLEINSERT, new
            {
              healthcareprofessionalid = healthCareProfessionalTitle.HealthCareProfessionalId,
              healthcareprofessionaltitledescription = healthCareProfessionalTitle.HealthCareProfessionalTitleDescription,
              modifiedbyuserid = healthCareProfessionalTitle.ModifiedByUserId
            }).ToList();
      }
      return DbManager.Connection.Query<HealthCareProfessionalTitle>(
        HealthCareProfessionalTitleConstants.HEALTHCAREPROFESSIONALTITLEUPDATE,
        new
        {
          healthcareprofessionaltitleid = healthCareProfessionalTitle.HealthCareProfessionalTitleId,
          healthcareprofessionalid = healthCareProfessionalTitle.HealthCareProfessionalId,
          healthcareprofessionaltitledescription = healthCareProfessionalTitle.HealthCareProfessionalTitleDescription,
          modifiedbyuserid = healthCareProfessionalTitle.ModifiedByUserId
        }
        ).ToList();
    }

    public IList<HealthCareProfessionalTitle> DeleteHealthCareProfessionalTitle(
      HealthCareProfessionalTitle healthCareProfessionalTitle)
    {
      return
        DbManager.Connection.Query<HealthCareProfessionalTitle>(
          HealthCareProfessionalTitleConstants.HEALTHCAREPROFESSIONALTITLEDELETE, new
          {
            healthcareprofessionaltitleid = healthCareProfessionalTitle.HealthCareProfessionalTitleId,
            healthcareprofessionalid = healthCareProfessionalTitle.HealthCareProfessionalId
          }).ToList();
    }

    public IList<HealthCareProfessionalTitle> GetHealthCareProfessionalTitles(int healthCareProfessionalId)
    {
      return
        DbManager.Connection.Query<HealthCareProfessionalTitle>(
          HealthCareProfessionalTitleConstants.HEALTHCAREPROFESSIONALTITLESBYHEALTHCAREPROFESSIONAL,
          new {healthcareprofessionalid = healthCareProfessionalId}).ToList();
    }

    public IList<HealthCareProfessionalAccreditation> GetHealthCareProfessionalAccreditations(
      int healthCareProfessionalId)
    {
      return
        DbManager.Connection.Query<HealthCareProfessionalAccreditation>(
          HealthCareProfessionalAccreditationConstants.HEALTHCAREPROFESSIONALACCREDITATIONSBYHEALTHCAREPROFESSIONAL,
          new
          {
            healthcareprofessionalid = healthCareProfessionalId
          }).ToList();
    }

    public IList<HealthCareProfessionalAccreditation> SaveHealthCareProfessionalAccreditation(
      HealthCareProfessionalAccreditation healthCareProfessionalAccreditation)
    {
      var healthCareProfessionalAccreditations =
         DbManager.Connection.Query<HealthCareProfessionalAccreditation>(
           HealthCareProfessionalAccreditationConstants.HEALTHCAREPROFESSIONALACCREDITATIONSBYHEALTHCAREPROFESSIONAL,
           new { healthcareprofessionalid = healthCareProfessionalAccreditation.HealthCareProfessionalId }).ToList();
      if (
        healthCareProfessionalAccreditations.Any(h => h.HealthCareProfessionalAccreditationTitle.ToLower() == healthCareProfessionalAccreditation.HealthCareProfessionalAccreditationTitle))
      {
        return healthCareProfessionalAccreditations;
      }
      if (healthCareProfessionalAccreditation.HealthCareProfessionalAccreditationId == 0)
      {
        return
          DbManager.Connection.Query<HealthCareProfessionalAccreditation>(
            HealthCareProfessionalAccreditationConstants.HEALTHCAREPROFESSIONALACCREDITATIONSINSERT, new
            {
              healthcareprofessionalid = healthCareProfessionalAccreditation.HealthCareProfessionalId,
              healthcareprofessionalaccreditationtitle = healthCareProfessionalAccreditation.HealthCareProfessionalAccreditationTitle,
              healthcareprofessionalaccreditationorganisation = healthCareProfessionalAccreditation.HealthCareProfessionalAccreditationOrganisation,
              modifiedbyuserid = healthCareProfessionalAccreditation.ModifiedByUserId
            }).ToList();
      }
      return DbManager.Connection.Query<HealthCareProfessionalAccreditation>(
        HealthCareProfessionalAccreditationConstants.HEALTHCAREPROFESSIONALACCREDITATIONSUPDATE,
        new
        {
          healthcareprofessionalaccreditationid = healthCareProfessionalAccreditation.HealthCareProfessionalAccreditationId,
          healthcareprofessionalid = healthCareProfessionalAccreditation.HealthCareProfessionalId,
          healthcareprofessionalaccreditationtitle = healthCareProfessionalAccreditation.HealthCareProfessionalAccreditationTitle,
          healthcareprofessionalaccreditationorganisation = healthCareProfessionalAccreditation.HealthCareProfessionalAccreditationOrganisation,
          modifiedbyuserid = healthCareProfessionalAccreditation.ModifiedByUserId
        }
        ).ToList();

    }

    public IList<HealthCareProfessionalAccreditation> DeleteHealthCareProfessionalAccreditation(
      HealthCareProfessionalAccreditation healthCareProfessionalAccreditation)
    {
      return new List<HealthCareProfessionalAccreditation>();
    }
  }
}

