﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Corsair;
using FosteringMinds.Models;

namespace FosteringMinds.Repositories.Tests
{
  [TestClass]
  public class HealthCareProfessionalRepositoryTests
  {
    IDbManager dbManager;
    IHealthCareProfessionalRepository healthCareProfessionalRepository;
    [TestInitialize]
    public void Setup()
    {
      dbManager = new DbManager.MySql.DbManager("fosteringminds");
      healthCareProfessionalRepository = new HealthCareProfessionalRepository(dbManager);
    }
    [TestMethod]
    public void SaveHealthCareProfessionalInsertTest()
    {
      var healthCareProfessional = new HealthCareProfessional();
      var datestamp = DateTime.Now.ToLongTimeString();
      var healthCareProfessionalFirstName = "Franz - Inserted XYZ - " + datestamp;
      var healthCareProfessionalLastName = "Ferdinand - Inserted XYZ - " + datestamp;
      var healthCareProfessionalTitle = "Mr - Inserted - " + datestamp;
      var healthCareProfessionalBio = "Test Bio - Inserted - " + datestamp;
      var healthCareProfessionalUrl = "http://www.something.net - Inserted - " + datestamp;
      var modifiedByUserId = "ac7da837-a4b5-4e17-a925-7a0d71ceddbc";

      healthCareProfessional.HealthCareProfessionalBio = healthCareProfessionalBio;
      healthCareProfessional.HealthCareProfessionalFirstName = healthCareProfessionalFirstName;
      healthCareProfessional.HealthCareProfessionalLastName = healthCareProfessionalLastName;
      healthCareProfessional.HealthCareProfessionalTitle = healthCareProfessionalTitle;
      healthCareProfessional.HealthCareProfessionalUrl = healthCareProfessionalUrl;
      healthCareProfessional.ModifiedByUserId = modifiedByUserId;

      var c = healthCareProfessionalRepository.SaveHealthCareProfessional(healthCareProfessional);
      Assert.IsNotNull(c);
      Assert.IsTrue(c.HealthCareProfessionalFirstName == healthCareProfessionalFirstName);
      Assert.IsTrue(c.HealthCareProfessionalLastName == healthCareProfessionalLastName);
      Assert.IsTrue(c.HealthCareProfessionalTitle == healthCareProfessionalTitle);
      Assert.IsTrue(c.HealthCareProfessionalUrl == healthCareProfessionalUrl);
      Assert.IsTrue(c.HealthCareProfessionalBio == healthCareProfessionalBio);
      Assert.IsTrue(c.HealthCareProfessionalId != 0);
      Assert.IsTrue(c.ModifiedByUserId == modifiedByUserId);
    }
    [TestMethod]
    public void SaveHealthCareProfessionalUpdateTest()
    {
      var healthCareProfessional = new HealthCareProfessional();
      var datestamp = DateTime.Now.ToLongTimeString();
      var healthCareProfessionalBio = "Test Bio - Updated " + datestamp;
      var healthCareProfessionalFirstName = "Candice - Updated " + datestamp;
      var healthCareProfessionalLastName = "Gillespie - Updated " + datestamp;
      var healthCareProfessionalTitle = "Ms";
      var healthCareProfessionalUrl = "https://candicegillespie.co.za";
      var healthCareProfessionalId = 23;

      healthCareProfessional.HealthCareProfessionalBio = healthCareProfessionalBio;
      healthCareProfessional.HealthCareProfessionalFirstName = healthCareProfessionalFirstName;
      healthCareProfessional.HealthCareProfessionalLastName = healthCareProfessionalLastName;
      healthCareProfessional.HealthCareProfessionalTitle = healthCareProfessionalTitle;
      healthCareProfessional.HealthCareProfessionalId = healthCareProfessionalId;
      healthCareProfessional.HealthCareProfessionalUrl = healthCareProfessionalUrl;
      var c = healthCareProfessionalRepository.SaveHealthCareProfessional(healthCareProfessional);
      Assert.IsNotNull(c);
      Assert.IsTrue(c.HealthCareProfessionalFirstName == healthCareProfessionalFirstName);
      Assert.IsTrue(c.HealthCareProfessionalLastName == healthCareProfessionalLastName);
      Assert.IsTrue(c.HealthCareProfessionalTitle == healthCareProfessionalTitle);
      Assert.IsTrue(c.HealthCareProfessionalBio == healthCareProfessionalBio);
      Assert.IsTrue(c.HealthCareProfessionalUrl == healthCareProfessionalUrl);
      Assert.IsTrue(c.HealthCareProfessionalId == healthCareProfessionalId);
    }
    [TestMethod]
    public void SaveHealthCareProfessionalValidationTest()
    {
      var healthCareProfessional = new HealthCareProfessional();
      var datestamp = DateTime.Now.ToLongTimeString();
      var healthCareProfessionalFirstName = string.Empty;
      var healthCareProfessionalLastName = string.Empty;
      var healthCareProfessionalBio = "Test Bio - Inserted - " + datestamp;
      var healthCareProfessionalUrl = "http://www.something.net - Inserted - " + datestamp;

      healthCareProfessional.HealthCareProfessionalBio = healthCareProfessionalBio;
      healthCareProfessional.HealthCareProfessionalFirstName = healthCareProfessionalFirstName;
      healthCareProfessional.HealthCareProfessionalLastName = healthCareProfessionalLastName;
      healthCareProfessional.HealthCareProfessionalUrl = healthCareProfessionalUrl;

      var c = healthCareProfessionalRepository.SaveHealthCareProfessional(healthCareProfessional);
      Assert.IsNull(c);
      Assert.IsNotNull(healthCareProfessionalRepository.ValidationResults);
    }
    [TestMethod]
    public void GetHealthCareProfessionalByIdTest()
    {
      var healthCareProfessional = healthCareProfessionalRepository.GetHealthCareProfessionalById(23);
      Assert.IsNotNull(healthCareProfessional);
      Assert.IsNotNull(healthCareProfessional.HealthCareProfessionalId);
      Assert.IsNotNull(healthCareProfessional.HealthCareProfessionalId == 1);
      Assert.IsNotNull(healthCareProfessional.HealthCareProfessionalFirstName, "Counsellor Name");
      Assert.IsNotNull(healthCareProfessional.HealthCareProfessionalBio, "Counsellor Bio");
      Assert.IsNotNull(healthCareProfessional.HealthCareProfessionalUrl, "Counsellor Domain");

      //Assert.IsNotNull(counsellor);
      //Assert.IsTrue(counsellor.CounsellorId == 1);
      //Assert.IsNotNull(counsellor.CounsellorDomain);
    }
    [TestMethod]
    public void GetHealthCareProfessionalByUrlTest()
    {
      var healthCareProfessional = healthCareProfessionalRepository.GetHealthCareProfessionalByUrl("https://candicegillespie.co.za");
      Assert.IsNotNull(healthCareProfessional);
      Assert.IsNotNull(healthCareProfessional.HealthCareProfessionalId);
      Assert.IsNotNull(healthCareProfessional.HealthCareProfessionalFirstName, "Counsellor Name");
      Assert.IsNotNull(healthCareProfessional.HealthCareProfessionalBio, "Counsellor Bio");
      Assert.IsNotNull(healthCareProfessional.HealthCareProfessionalUrl, "Counsellor Domain");
    }
    [TestMethod]
    public void GetHealthCareProfessionalsTest()
    {
      var c = healthCareProfessionalRepository.GetHealthCareProfessionals();
      Assert.IsNotNull(c);
      Assert.IsTrue(c.Count > 0);
    }

    [TestMethod]
    public void SaveHealthCareProfessionalTitleInsert()
    {
      var healthCareProfessionalTitle = new HealthCareProfessionalTitle
      {
        HealthCareProfessionalTitleDescription = "Soul Smuggler",
        HealthCareProfessionalId = 23,
        ModifiedByUserId = "ac7da837-a4b5-4e17-a925-7a0d71ceddbc"
      };
      var c = healthCareProfessionalRepository.SaveHealthCareProfessionalTitle(healthCareProfessionalTitle);
      Assert.IsNotNull(c);
      Assert.IsTrue(c.Count > 0);
    }
    [TestMethod]
    public void SaveHealthCareProfessionalTitleUpdate()
    {
      var healthCareProfessionalTitle = new HealthCareProfessionalTitle
      {
        HealthCareProfessionalTitleId = 1,
        HealthCareProfessionalTitleDescription = "Soul Smuggler 123",
        HealthCareProfessionalId = 23,
        ModifiedByUserId = "ac7da837-a4b5-4e17-a925-7a0d71ceddbc"
      };
      var c = healthCareProfessionalRepository.SaveHealthCareProfessionalTitle(healthCareProfessionalTitle);
      Assert.IsNotNull(c);
      Assert.IsTrue(c.Count > 0);
    }
    [TestMethod]
    public void DeleteHealthCareProfessionalTitleTest()
    {
      var healthCareProfessionalTitle = new HealthCareProfessionalTitle
      {
        HealthCareProfessionalTitleId = 1,
        HealthCareProfessionalTitleDescription = "Soul Smuggler 123",
        HealthCareProfessionalId = 23,
        ModifiedByUserId = "ac7da837-a4b5-4e17-a925-7a0d71ceddbc"
      };
      var c = healthCareProfessionalRepository.DeleteHealthCareProfessionalTitle(healthCareProfessionalTitle);
      Assert.IsNotNull(c);
      Assert.IsTrue(c.Where(k => k.HealthCareProfessionalTitleDescription == healthCareProfessionalTitle.HealthCareProfessionalTitleDescription).Count() == 0);
    }

    [TestMethod]
    public void GetHealthCareProfessionalTitlesTest()
    {
      var c = healthCareProfessionalRepository.GetHealthCareProfessionalTitles(23);
      Assert.IsNotNull(c);
      Assert.IsTrue(c.Count > 0);
    }
    [TestMethod]
    public void SaveHealthCareProfessionalAccreditationInsert()
    {
      var healthCareProfessionalAccreditation = new HealthCareProfessionalAccreditation()
      {
        HealthCareProfessionalAccreditationTitle = "Soul Smuggler",
        HealthCareProfessionalAccreditationOrganisation = "Soul inc",
        HealthCareProfessionalId = 23,
        ModifiedByUserId = "ac7da837-a4b5-4e17-a925-7a0d71ceddbc"
      };
      var c = healthCareProfessionalRepository.SaveHealthCareProfessionalAccreditation(healthCareProfessionalAccreditation);
      Assert.IsNotNull(c);
      Assert.IsTrue(c.Count > 0);
    }
    [TestMethod]
    public void SaveHealthCareProfessionalAccreditationUpdate()
    {
      var healthCareProfessionalAccreditation = new HealthCareProfessionalAccreditation()
      {
        HealthCareProfessionalAccreditationId = 1,
        HealthCareProfessionalAccreditationTitle = "Soul Smuggler 123",
        HealthCareProfessionalAccreditationOrganisation = "Soul inc 123",
        HealthCareProfessionalId = 23,
        ModifiedByUserId = "ac7da837-a4b5-4e17-a925-7a0d71ceddbc"
      };
      var c = healthCareProfessionalRepository.SaveHealthCareProfessionalAccreditation(healthCareProfessionalAccreditation);
      Assert.IsNotNull(c);
      Assert.IsTrue(c.Count > 0);
    }
    [TestMethod]
    public void GetHealthCareProfessionalAccreditationsTest()
    {
      var c = healthCareProfessionalRepository.GetHealthCareProfessionalAccreditations(23);
      Assert.IsNotNull(c);
      Assert.IsTrue(c.Count > 0);
    }
    public void DeleteHealthCareProfessionalAccreditationTest()
    {
      var healthCareProfessionalAccreditation = new HealthCareProfessionalAccreditation()
      {
        HealthCareProfessionalAccreditationId = 1,
        HealthCareProfessionalAccreditationTitle = "Soul Smuggler 123",
        HealthCareProfessionalAccreditationOrganisation = "Soul inc 123",
        HealthCareProfessionalId = 23,
        ModifiedByUserId = "ac7da837-a4b5-4e17-a925-7a0d71ceddbc"
      };
      var c = healthCareProfessionalRepository.DeleteHealthCareProfessionalAccreditation(healthCareProfessionalAccreditation);
      Assert.IsNotNull(c);
      Assert.IsTrue(c.Where(k => k.HealthCareProfessionalAccreditationTitle == healthCareProfessionalAccreditation.HealthCareProfessionalAccreditationTitle).Count() == 0);
    }
  }
}
