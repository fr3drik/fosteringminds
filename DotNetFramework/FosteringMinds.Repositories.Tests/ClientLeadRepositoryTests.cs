﻿using System;
using Corsair;
using FosteringMinds.Models;
using FosteringMinds.Repositories.Interfaces;
using FosteringMinds.Repositories.Repos;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace FosteringMinds.Repositories.Tests
{
  [TestClass]
  public class ClientLeadRepositoryTests
  {
    IDbManager dbManager;
    IClientLeadRepository clientLeadRepository;
    [TestInitialize]
    public void Setup()
    {
      dbManager = new DbManager.MySql.DbManager("fosteringminds");
      clientLeadRepository = new ClientLeadRepository(dbManager);
    }
    [TestMethod]
    public void SaveClientLeadTypeInsertTest()
    {
      var clientLeadType = new ClientLeadType();
      clientLeadType.ClientLeadTypeDescription = "Email";
      clientLeadType.ModifiedByUserId = "ac7da837-a4b5-4e17-a925-7a0d71ceddbc";
      var clt = clientLeadRepository.SaveClientLeadType(clientLeadType);
      Assert.IsNotNull(clt);
    }
    [TestMethod]
    public void SaveClientLeadTypeInsertDuplicateTest()
    {
      var clientLeadType = new ClientLeadType();
      clientLeadType.ClientLeadTypeDescription = "Email";
      clientLeadType.ModifiedByUserId = "ac7da837-a4b5-4e17-a925-7a0d71ceddbc";
      var clt = clientLeadRepository.SaveClientLeadType(clientLeadType);
      var c = clientLeadRepository.GetClientLeadTypesByDescription(clientLeadType.ClientLeadTypeDescription);
      Assert.IsTrue(c.Count() == 1);
    }
    [TestMethod]
    public void GetClientLeadTypeByDescriptionTest()
    {
      var clientLeadTypeDescription = "Email";
      var clt = clientLeadRepository.GetClientLeadTypeByDescription(clientLeadTypeDescription);
      Assert.IsNotNull(clt);
    }

    [TestMethod]
    public void SaveClientLeadInsertTest()
    {
      var clientLead = new ClientLead();
      clientLead.ClientLeadTypeId = 10;
      clientLead.ClientFirstName = "Test";
      clientLead.ClientSurname = "Lead";
      clientLead.ContactNumber = "0111231234";
      clientLead.EmailAddress = "fredrik@test.co.za";
      clientLead.GeoLocation = "";
      clientLead.HealthCareProfessionalId = 23;
      clientLead.Message = "Test Message";
      clientLead.ModifiedByUserId = "ac7da837-a4b5-4e17-a925-7a0d71ceddbc";
      clientLead.Referrer = "www.google.co.za";
      var c = clientLeadRepository.SaveClientLead(clientLead);
      Assert.IsNotNull(c);
    }
    [TestMethod]
    public void SaveClientLeadValidationTest()
    {
      var clientLead = new ClientLead();
      clientLead.ClientLeadTypeId = 10;
      clientLead.ClientFirstName = "";
      clientLead.ContactNumber = "011123123";
      clientLead.EmailAddress = "fredrik@test.co.za";
      clientLead.GeoLocation = "";
      clientLead.HealthCareProfessionalId = 23;
      clientLead.Message = "Test Message";
      clientLead.ModifiedByUserId = "ac7da837-a4b5-4e17-a925-7a0d71ceddbc";
      var c = clientLeadRepository.SaveClientLead(clientLead);
      Assert.IsNotNull(clientLeadRepository.ValidationResults.Count > 0);
    }
    [TestMethod]
    public void SaveClientLeadUpdateTest()
    {
      var clientLead = new ClientLead();
      clientLead.ClientLeadId = 1;
      clientLead.ClientLeadTypeId = 10;
      clientLead.ClientFirstName = "Test Client Name -- update";
      clientLead.ContactNumber = "0111231234";
      clientLead.EmailAddress = "fredrik@test.co.za";
      clientLead.GeoLocation = "";
      clientLead.HealthCareProfessionalId = 23;
      clientLead.Message = "Test Message";
      clientLead.ModifiedByUserId = "ac7da837-a4b5-4e17-a925-7a0d71ceddbc";
      var c = clientLeadRepository.SaveClientLead(clientLead);
      Assert.IsNotNull(c);
    }

    [TestMethod]
    public void GetClientLeadsByHealthCareProfessionalTest()
    {
      var clientLeads = clientLeadRepository.GetClientLeadsByHealthCareProfessional(23);
      Assert.IsNotNull(clientLeads);
    }
  }
}
