﻿using FosteringMinds.Repositories;
using System.Web.Mvc;
using FosteringMinds.IdentityConfig;
using FosteringMinds.Models;
using FosteringMinds.Repositories.Interfaces;
using FosteringMinds.Repositories.Repos;
using FosteringMinds.WebUI.Models;
using FosteringMinds.WebUI.SharedModels;

namespace FosteringMinds.WebUI.Controllers
{
  [Authorize]
  public class HomeController : Controller
  {
    private IHealthCareProfessionalRepository healthCareProfessionalRepository;
    private IClientLeadRepository clientLeadRepository;
    public HomeController()
    {
      healthCareProfessionalRepository = new HealthCareProfessionalRepository(ApplicationDbManager.Instance);
      clientLeadRepository = new ClientLeadRepository(ApplicationDbManager.Instance);
    }
    // GET: Home
    public ActionResult Index(string viewName)
    {
      if (string.IsNullOrEmpty(viewName))
        viewName = "healthCareProfessionals";
      AdminModel adminModel = new AdminModel(viewName, ControllerContext, healthCareProfessionalRepository);
      return View(adminModel);
    }
    public ActionResult HealthCareProfessional(string viewName, int healthCareProfessionalId)
    {
      if (string.IsNullOrEmpty(viewName))
        viewName = "healthCareProfessional";
      AdminModel adminModel = new AdminModel(viewName, healthCareProfessionalId, ControllerContext, healthCareProfessionalRepository);
      return View("Index", adminModel);
    }

    [HttpPost]
    public JsonResult SaveHealthCareProfessional(HealthCareProfessional healthCareProfessional)
    {
      AdminModel adminModel = new AdminModel("healthCareProfessionalForm", healthCareProfessional, ControllerContext, healthCareProfessionalRepository);
      adminModel.SaveHealthCareProfessional();
      return Json(new
      {
        adminModel.HealthCareProfessional,
        adminModel.ViewHtml,
        adminModel.ViewName
      });
    }

    public ActionResult ClientLeads(int healthCareProfessionalId)
    {
      AdminModel adminModel = new AdminModel("clientLeads", healthCareProfessionalId, ControllerContext, healthCareProfessionalRepository, clientLeadRepository);
      return View("Index", adminModel);
    }

    public ActionResult ClientLead(int healthCareProfessionalId, int clientLeadId)
    {
      AdminModel model = new AdminModel("clientLead", healthCareProfessionalId, clientLeadId, ControllerContext, healthCareProfessionalRepository, clientLeadRepository);
      return View("Index", model);
    }
  }
}