﻿using Microsoft.Owin;
using Owin;
[assembly: OwinStartup(typeof(FosteringMinds.AdminWebUI.StartUp))]
namespace FosteringMinds.AdminWebUI
{
  public partial class StartUp
  {
    public void Configuration(IAppBuilder app)
    {
      // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
      ConfigureAuth(app);
    }
  }
}