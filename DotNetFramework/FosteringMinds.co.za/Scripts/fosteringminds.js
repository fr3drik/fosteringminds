﻿$(document).on("click",
  "#btnSaveHealthCareProfessional",
  function (e) {
    var healthCareProfessional = {
      HealthCareProfessionalId: 0,
      HealthCareProfessionalFirstName: '',
      HealthCareProfessionalLastName: '',
      HealthCareProfessionalTitle: '',
      HealthCareProfessionalBio: '',
      HealthCareProfessionalUrl: ''
    };
    healthCareProfessional.HealthCareProfessionalId = $("#healthCareProfessionalId").val();
    healthCareProfessional.HealthCareProfessionalFirstName = $("#firstName").val();
    healthCareProfessional.HealthCareProfessionalLastName = $("#lastName").val();
    healthCareProfessional.HealthCareProfessionalTitle = $("#title").val();
    healthCareProfessional.HealthCareProfessionalBio = $("#bio").val();
    healthCareProfessional.HealthCareProfessionalUrl = $("#url").val();
    $.ajax({
      type: "POST",
      url: "SaveHealthCareProfessional",
      data: JSON.stringify(healthCareProfessional),
      success: function (data) {
        $("#healthCareProfessionalContainer").empty();
        $("#healthCareProfessionalContainer").html(data.ViewHtml);
      },
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    });
  });
$(document).on("click", "#btnSaveLead", function(e) {
  var clientLead = {
    HealthCareProfessionalId: 0,
    ClientLeadTypeId: 0,
    ClientLeadId: 0,
    ClientFirstName: '',
    ClientSurname: '',
    ContactNumber: '',
    EmailAddress: '',
    Referrer: '',
    ClientLeadTypeDescription: '',
    LeadStatus: 0,
    ClientLeadTypeId: 0,
    Message: ''
  };
  clientLead.HealthCareProfessionalId = parseInt($("#healthCareProfessionalId").val());
  clientLead.ClientFirstName = $("#leadClientFirstName").val();
  clientLead.ClientSurname = $("#leadClientSurname").val();
  clientLead.ContactNumber = $("#leadContactNumber").val();
  clientLead.EmailAddress = $("#leadEmailAddress").val();
  clientLead.Referrer = $("#leadReferrer").val();
  clientLead.ClientLeadTypeDescription = $("#leadTypeDescription").val();
  clientLead.LeadStatus = $("#leadStatus").val();
  clientLead.ClientLeadId = parseInt($("#clientLeadId").val());
  clientLead.ClientLeadTypeId = parseInt($("#clientLeadTypeId").val());
  clientLead.Message = $("#leadMessage").val();

  $.ajax({
    type: "POST",
    url: "SaveClientLead",
    data: JSON.stringify(clientLead),
    success: function (data) {
      $("#leadModalBody").empty();
      $("#leadModalBody").html(data.ViewHtml);
      if (data.ModelIsValid) {
        $("#modelStateIndicator").show();
      } else {
        $("#modelStateIndicator").hide();
      }
    },
    contentType: "application/json; charset=utf-8",
    dataType: "json"
  });
});
$(document).on("click", "#btnCloseLeadModal", function(e) {
  var healthCareProfessionalId = parseInt($("#healthCareProfessionalId").val());
  $.ajax({
    type: "GET",
    url: "GetClientLeads?healthCareProfessionalId=" + healthCareProfessionalId,
    success: function (data) {
      $("#clientLeadsContainer").empty();
      $("#clientLeadsContainer").html(data.ViewHtml);
    },
    contentType: "application/json; charset=utf-8",
    dataType: "json"
  });
});
$(document).on("click", ".btnEditClientLead", function (e) {
  var healthCareProfessionalId = $(this).data('healthCareProfessionalId');
  var clientLeadId = $(this).data('clientLeadId');
  $.ajax({
    type: "GET",
    url: "GetClientLead?healthCareProfessionalId=" + healthCareProfessionalId + "&clientLeadId=" + clientLeadId,
    success: function (data) {
      if (data.ModelIsValid) {
        $("#modelStateIndicator").show();
      } else {
        $("#modelStateIndicator").hide();
      }
      $("#leadModalBody").empty();
      $("#leadModalBody").html(data.ViewHtml);
      $('#leadModal').modal();
    },
    contentType: "application/json; charset=utf-8",
    dataType: "json"
  });
});
$(document).on("change", "#chkLeadStatusPending", function (e) { $("#leadStatus").val(0); });
$(document).on("change", "#chkLeadStatusFollowedUp", function (e) { $("#leadStatus").val(1); });
$(document).on("click", ".btnEditHealthCareProfessionalTitle", function (e) {
  var healthCareProfessionalId = parseInt($("#healthCareProfessionalId").val());
  var healthCareProfessionalTitleId = parseInt($(this).data('healthCareProfessionalTitleId'));
  var editRowId = "#editRow" + healthCareProfessionalId + "_" + healthCareProfessionalTitleId;
  var viewRowId = "#viewRow" + healthCareProfessionalId + "_" + healthCareProfessionalTitleId;
  $(editRowId).show();
  $(viewRowId).hide();
});
$(document).on("click", ".btnCancelEditHealthCareProfessionalTitle", function (e) {
  var healthCareProfessionalId = parseInt($("#healthCareProfessionalId").val());
  var healthCareProfessionalTitleId = parseInt($(this).data('healthCareProfessionalTitleId'));
  var editRowId = "#editRow" + healthCareProfessionalId + "_" + healthCareProfessionalTitleId;
  var viewRowId = "#viewRow" + healthCareProfessionalId + "_" + healthCareProfessionalTitleId;
  $(editRowId).hide();
  $(viewRowId).show();
});
$(document).on("click", ".btnUpdateHealthCareProfessionalTitle", function(e) {
  var healthCareProfessionalId = parseInt($("#healthCareProfessionalId").val());
  var healthCareProfessionalTitleId = parseInt($(this).data('healthCareProfessionalTitleId'));
  var inputId = "#input" + healthCareProfessionalId + "_" + healthCareProfessionalTitleId;
  var healthCareProfessionalTitleDescription = $(inputId).val();
  var healthCareProfessionalTitle = {
    HealthCareProfessionalId: 0,
    HealthCareProfessionalTitleId: 0,
    HealthCareProfessionalTitleDescription: ''
  };
  healthCareProfessionalTitle.HealthCareProfessionalId = healthCareProfessionalId;
  healthCareProfessionalTitle.HealthCareProfessionalTitleId = healthCareProfessionalTitleId;
  healthCareProfessionalTitle.HealthCareProfessionalTitleDescription = healthCareProfessionalTitleDescription;
  $.ajax({
    type: "POST",
    url: "SaveHealthCareProfessionalTitle",
    data: JSON.stringify(healthCareProfessionalTitle),
    success: function (data) {
      $("#healthCareProfessionalContainer").empty();
      $("#healthCareProfessionalContainer").html(data.ViewHtml);
    },
    contentType: "application/json; charset=utf-8",
    dataType: "json"
  });
});
$(document).on("click", "#btnSaveHealthCareProfessionalTitle", function(e) {
  var healthCareProfessionalId = parseInt($("#healthCareProfessionalId").val());
  var healthCareProfessionalTitleId = parseInt($(this).data('healthCareProfessionalTitleId'));
  var inputId = "#healthCareProfessionalTitle";
  var healthCareProfessionalTitleDescription = $(inputId).val();
  var healthCareProfessionalTitle = {
    HealthCareProfessionalId: 0,
    HealthCareProfessionalTitleId: 0,
    HealthCareProfessionalTitleDescription: ''
  };
  healthCareProfessionalTitle.HealthCareProfessionalId = healthCareProfessionalId;
  healthCareProfessionalTitle.HealthCareProfessionalTitleId = healthCareProfessionalTitleId;
  healthCareProfessionalTitle.HealthCareProfessionalTitleDescription = healthCareProfessionalTitleDescription;
  $.ajax({
    type: "POST",
    url: "SaveHealthCareProfessionalTitle",
    data: JSON.stringify(healthCareProfessionalTitle),
    success: function (data) {
      $("#healthCareProfessionalContainer").empty();
      $("#healthCareProfessionalContainer").html(data.ViewHtml);
    },
    contentType: "application/json; charset=utf-8",
    dataType: "json"
  });
});