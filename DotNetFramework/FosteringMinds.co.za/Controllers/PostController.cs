﻿using FosteringMinds.co.za.Models.MarkdownSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FosteringMinds.co.za.Controllers
{
    public class PostController : Controller
    {
		Markdown md;
		PostModel pm;
		[ValidateInput(false)]
		// GET: Post
		public ActionResult Index()
        {
			md = new Markdown();
			string markdownContent = System.IO.File.ReadAllText(@"C:\Projects\Journals\FosteringMinds.Posts.md");
			pm = new PostModel(md, markdownContent);
            return View(pm);
        }
    }

	public class PostModel
	{
		private Markdown markdown;
		public string PostContent { get; set; }
		public PostModel(Markdown markdown, string markdownContent)
		{
			this.markdown = markdown;
			PostContent = markdown.Transform(markdownContent);
		}
	}
}